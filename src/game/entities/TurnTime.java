package game.entities;

import bitzero.server.config.ConfigHandle;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class TurnTime {
    public static final int TIME_TO_NEW_GAME = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "TIME_TO_NEW_GAME");//dem nguoc auto start
    public static final int TIME_TO_BET_START = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "TIME_TO_BET_START");//Dem nguoc de bat dau ca do -> Thoi gian dien chia bai
    public static final int TIME_TO_BET_END = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "TIME_TO_BET_END");//Thoi gian ca do: dat cuoc, danh ga, danh bien...
    public static final int TIME_TO_GAME_RESULT = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "TIME_TO_GAME_RESULT");;//Thoi gian nan bai
    public static final int TIME_TO_GAME_END = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "TIME_TO_GAME_END");//So bai
    public static final int CHOICE_DEALER = ConfigHandle.instance().getInt(ServerConstant.GAME_NAME, "CHOICE_DEALER");
}
