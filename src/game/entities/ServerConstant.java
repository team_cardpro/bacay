package game.entities;

import bitzero.server.config.ConfigHandle;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServerConstant {
	public static final String GAME_NAME = ConfigHandle.instance().get("games");
	// Tung
	public static final List<RoomInfo> roomInfos = new ArrayList<RoomInfo>();
	public static final int NUM_AFK = ConfigHandle.instance().getInt(GAME_NAME,"NUM_AFK");//So van afk
	// Tung end

	public static JSONObject configMobile;
	public static JSONObject config;
	public static JSONObject configBet;
	public static JSONArray configAward;
	public static JSONObject configLevel;
	public static JSONObject configPayment;
	public static JSONObject botConfig;

	// public static final long MAX_ROOM =
	// ConfigHandle.instance().getLong("Pokerhk", "MaxRoom");
	public static final int MAX_CARDS = 36;
	public static final int MAX_CARD_PER_PLAYER_IN_GAME = 3;// So bai toi da
															// trong van
	public static final int MAX_CARD_PER_PLAYER_IN_CHOICE_DEALER = 1;// So la
																		// bai
																		// toi
																		// da de
																		// thi
																		// chuong
	public static final int MIN_PLAYER = 2;
	// public static final int MAX_PLAYER = 6;
	public static final int MAX_PLAYER = ConfigHandle.instance().getInt(GAME_NAME, "MaxUserInRoom");
	public static final int TIMES_MIN_BET = 3;
	public static final int TIMES_MIN_BET2 = 10;
	public static final int MAX_ROOM_VIEWER = 4;
	public static final int GAME_TIME = 64;
	public static final int GAME_TIME2 = 64;
	public static final int TIME_REALSE_HOLD = 120;
	// public static final int GAME_TIME = 94;
	public static final int TIME_SOCHI = 3;
	public static final int TIME_SOBAI = 6;
	public static final int TIME_SUMARY = 7;
	// public static final int TIME_RESULT = 87;
	public static final int SOBAI_CHI = 0;
	public static final int SOBAI_MAUBINH = 1;
	public static final int SOBAI_BINHLUNG = 2;
	public static final int XAPBAI = 3;
	public static final int SUMMARY = 4;
	public static final int JACKPOT = 5;
	public static final int ENDGAME = 6;
	public static final int NONE = 7;

	public static final int STEP_GAME_RESULT = 30;

	public static final int USER_TIME_OUT = 75; // thoi gian ko lam gi trong van
												// choi

	// state ingame BinhAt
	public static final int BINH_BAI = 8;
	public static final int BINH_CHI = 9;
	public static final int BINH_XAP_BAI = 10;
	public static final int BINH_XAP_LANG = 11;
	public static final int BINH_TINH_AT = 12;
	public static final int BINH_SUMMARY = 13;
	public static final int BINH_JACKPOT = 14;
	public static final int BINH_END_GAME = 15;

	public static final String GAME_ID = ConfigHandle.instance().get("gameId");// "Tala";//Binh

	public static final int BET_TYPE_BEAN = 1;
	public static final int BET_TYPE_COIN = 2;
	public static final String PLAYER_SESSIONKEY = "player_sessionkey";
	public static final String PLAYER_CHAIR = "player_chair";
	public static final String PLAYER_VIEWCHAIR = "player_viewchair";
	public static final String PLAYER_CHANNEL = "player_channel";
	public static final String PLAYER_INFO = "player_info";
	public static final String PLAYER_INIT = "player_init";
	public static final String PLAYER_RECONNECT = "player_reconnect";
	// public static final String FRIEND_GIFT = "friend_gift";
	// public static final String MESSAGE = "message";
	public static final String SHOP_MODULE_CAN_BONUS = "ShopCanBonus";

	public static final String USER_INFO = "user_info";

	public static final String CHANNEL_LOGIC = "channel_logic";

	public static final String ROOM_TYPE = "room_type";
	public static final String ROOM_LOGIC = "room_logic";
	public static final String ROOM_STATUS = "room_status";
	public static final String CREATE_ROOM_TIME = "create_room_time";
	public static final int eROOM_WAITING = 0;
	public static final int eROOM_PLAYING = 1;
	public static final int eROOM_CLOSED = 2;

	public static final String CONST_WEB_ROOT = ConfigHandle.instance().get(ServerConstant.GAME_ID, "web_root_static");
	public static final boolean IS_CHEAT = (ConfigHandle.instance().getLong("isCheat") == 1);
	public static final boolean IS_PURCHASE = (ConfigHandle.instance().getLong("isPurchase") == 1);
	public static final boolean IS_METRICLOG = (ConfigHandle.instance().getLong("isMetriclog") == 1);
	public static final boolean IS_EVENT_CAM_GIOI = (ConfigHandle.instance().getLong("isCamGioiEvent") == 1);
	public static final boolean ENABLE_BOT = ConfigHandle.instance().getLong("enable_bot") == 1;
	public static final boolean IS_DEBUG_TRACE = (ConfigHandle.instance().getLong("debug_trace") == 1);

	public static final boolean IS_GAMELOG = false;

	// public static final eRoomStatus STATUS;
	// SUFFIX AND PREFIX FOR KEY IN DATABASE
	// public static final String SUFFIX_FRIEND_GIFT = "_friend_gift";// suffix
	// luu gia tri friend gift trong database
	public static final String SUFFIX_MESSAGE = "_message";// suffix luu gia tri
															// message trong
															// database
	public static final String SUFFIX_PLAYER_INFO = "_player_info"; // suffix
																	// luu gia
																	// tri
																	// player
																	// info
																	// trong
																	// database
	public static final String SUFFIX_FRIEND_LIST = "_friend_list"; // suffix
																	// luu list
																	// friend
																	// trong
																	// cache
	public static final String SUFFIX_DEVICE_INFO = "_device_info"; // suffix
																	// luu gia
																	// tri
																	// player
																	// info
																	// trong
																	// database

	public static final String SUFFIX_ITEM = "_item"; // suffix luu gia tri
														// message trong
														// database
	public static final String SUFFIX_EVENT_SUMMER = "_eventsummer"; // suffix
																		// luu
																		// gia
																		// tri
																		// message
																		// trong
																		// database
	public static final String SUFFIX_POINT_ADD_G = "_pointAddG"; // suffix luu
																	// gia tri
																	// so G nap
																	// vao cua
																	// user
	public static final String SUFFIX_GIFT_CODE = "_gift_code"; // suffix luu
																// list friend
																// trong cache
	public static final String SUFFIX_GIFT_CODE_CAMGIOI = "_gift_code_CamGioi"; // suffix
																				// luu
																				// list
																				// friend
																				// trong
																				// cache
	public static final String SUFFIX_MINIGAME_SCRATCHCARD = "_mngame_scratch_card"; //
	public static final String SUFFIX_LEVEL_INFO = "_suffix_level_info"; //
	public static final String SUFFIX_SMS = "_sms_G"; // suffix luu gia tri G
														// khi nap Gold tu tin
														// nhan SMS

	public static final int TABLE_NEWBIE = 0;
	public static final int TABLE_ADVANCE = 1;
	public static final int AUTO_START_TIME = 5;

	public static final int CHANNEL_MAX_USER = 1500;
	public static final int CHANNEL_MAX_ROOM = 300;
	public static final int CHANNEL_TOP_NUM = 10;

	public static final int ROOM_MAXUSER = 100;

	public static final String DEVICE_ID = "device_id";
	public static final String DEVICE_INFO = "device_info";

	// select group channel err
	public static final int SELECT_CHANNEL_NOT_ENOUGHT_MONEY = 1;
	public static final int SELECT_CHANNEL_FULL_USER = 2;
	public static final int SELECT_CHANNEL_IN_ROOM = 3;
	// create room err
	public static final int CREATE_ROOM_NOT_ENOUGHT_MONEY = 1;
	public static final int CREATE_ROOM_CHANNEL_FULL = 2;
	public static final int CREATE_ROOM_WRONG_BET = 3;
	public static final int CREATE_TIME_OUT = 4;
	public static final int CREATE_ROOM_IS_JOIN = 5;
	public static final int CREATE_ROOM_NOT_JOIN_ZONE = 6;
	public static final int CREATE_LOW = 7;
	public static final int CREATE_MOBILE_ADVANCE = 7;
	// join room err
	public static final int JOINROOM_NOT_ENOUGHT_MONEY = 0;
	public static final int JOINROOM_WRONG_PASS = 1;
	public static final int JOINROOM_FULL = 2;
	public static final int JOINROOM_SAME_ROOM = 3;
	public static final int JOINROOM_DELETED = 4;
	public static final int JOINROOM_LOW = 5;
	public static final int JOINROOM_FROM_ROOM = 6;
	public static final int JOINROOM_NOT_ENOUGH_CHANNEL = 7;
	public static final int JOINROOM_NOT_IN_CHANNEL = 8;
	public static final int JOINROOM_EXIST_ROOM = 9;
	public static final int JOINROOM_MOBILE = 10;
	public static final int JOINROOM_ERROR = 11;

	// viewgame take chair err
	public static final int TAKECHAIR_NONE_ERR = 0;
	public static final int TAKECHAIR_NONE_CHAIR = 1; // da co nguoi dang ky ghe
														// nay
	public static final int TAKECHAIR_NONE_VIEWER = 2; // khong co trong danh
														// sach viewgame
	public static final int TAKECHAIR_FULL_VIEWER = 3; // khong co trong danh
														// sach viewgame
	public static final int TAKECHAIR_CHAIR_ERROR = 4; // khong co trong danh
														// sach viewgame

	public static final int QUICK_PLAY_CAN_NOT_CREATE_TABLE = 1;
	public static final int QUICK_PLAY_CAN_CREATE_TABLE = 2;

	public static final int CHAN_KENH = 1000000;

	public ServerConstant() {
		super();
	}
}
