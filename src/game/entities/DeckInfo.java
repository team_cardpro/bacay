package game.entities;

/**
 * Created by mrtun9z on 3/31/16.
 */
public class DeckInfo {
    public byte chair;
    public boolean isShowCard;
    public byte[] cardIds;

    public boolean isBet;
    public long goldBet;

    public boolean isChickedJoined;

    public BetExtraInfo betExtraInfo = new BetExtraInfo();
}
