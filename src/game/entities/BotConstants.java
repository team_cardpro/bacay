package game.entities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bitzero.util.common.business.CommonHandle;
import game.utils.Utils;

public class BotConstants {
	public static int MAX_ROOM_BOT = 10;
	public static int MIN_GAME_REMAIN = 1;
	public static int MAX_GAME_REMAIN = 5;
	public static int MIN_TIME_READY = 10;
	public static int MAX_TIME_READY = 40;
	public static final int MIN_GOLD = 5;
	public static final int MAX_GOLd = 10;

	public static void init() {
		// config
		loadConfig();
		// config
		try {
			MAX_ROOM_BOT = ServerConstant.botConfig.getInt("max_room_bot");
			MIN_GAME_REMAIN = ServerConstant.botConfig.getInt("min_game_quit");
			MAX_GAME_REMAIN = ServerConstant.botConfig.getInt("max_game_quit");
			MIN_TIME_READY = ServerConstant.botConfig.getInt("min_time_ready");
			MAX_TIME_READY = ServerConstant.botConfig.getInt("max_time_ready");
//			MIN_GOLD = ServerConstant.botConfig.getInt("min_gold");
//			MAX_GOLd = ServerConstant.botConfig.getInt("max_gold");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loadConfig() {
		String path = System.getProperty("user.dir");
		File file = new File(path + "/conf/bot.json");
		StringBuffer contents = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String text = null;
			while ((text = reader.readLine()) != null) {
				contents.append(text).append(System.getProperty("line.separator"));
			}
			ServerConstant.botConfig = new JSONObject(contents.toString());
		} catch (FileNotFoundException e) {
			CommonHandle.writeErrLog(e);
		} catch (IOException e) {
			CommonHandle.writeErrLog(e);
		} catch (JSONException e) {
			e.printStackTrace();
			CommonHandle.writeErrLog(e);
		}
	}

	public static String randRoomName() {
		try {
			JSONArray arr = ServerConstant.botConfig.getJSONArray("room");
			if (arr.length() > 0) {
				int idx = Utils.randomInRange(0, arr.length() - 1);
				return arr.getString(idx);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			CommonHandle.writeErrLog(e);
		}
		return "";
	}
}
