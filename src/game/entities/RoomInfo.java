package game.entities;

import game.server.GameManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class RoomInfo {
    public volatile byte dealerPlayerId = GameManager.DEALER_UNKNOW;
    public int curMaxBet;

    public int comission;// He so phe


    public int minBet;// = base
    public int maxBet;// = base * hsMaxBet -> muc cuoc toi da
    public int hsCalculatorMaxBet;//he so tinh muc cuoc toi da
    public int chickenBet;// = base * hsChickenBet -> muc cuoc ga

    public long goldDealer;// = base * hsDealer -> So tien de lam chuong
    public long goldPlayer;// = base * hsPlayer -> So tien de vao choi

    public long[] betExtraList;// = base * hsExtraBet -> Muc Cuoc bien

    public int hs10;// He so nhan tien khi 10
    public int hsSap;// He so nhan tien khi SAP


    private int _hsMaxBet;
    private int _hsChickenBet;
    private int[] _hsExtraBet;
    private int _hsGoldDealer;
    private int _hsGoldPlayer;


    public RoomInfo(JSONObject jsonObject) throws JSONException {
        comission = jsonObject.getInt("comission");
        minBet = jsonObject.getInt("base");
        _hsMaxBet = jsonObject.getInt("hsMaxBet");
        _hsChickenBet = jsonObject.getInt("hsChickenBet");
        _hsGoldDealer = jsonObject.getInt("goldDealer");
        _hsGoldPlayer = jsonObject.getInt("goldPlayer");

        maxBet = minBet * _hsMaxBet;
        chickenBet = minBet * _hsChickenBet;

        goldDealer = minBet * _hsGoldDealer;
        goldPlayer = minBet * _hsGoldPlayer;

        JSONArray arrayExtraBet = jsonObject.getJSONArray("hsExtraBet");
        betExtraList = new long[arrayExtraBet.length()];
        _hsExtraBet = new int[arrayExtraBet.length()];
        for (int i = 0; i < betExtraList.length; i++){
            _hsExtraBet[i] = arrayExtraBet.getInt(i);
//            betExtraList[i] = arrayExtraBet.getInt(i) * minBet;
        }

        hs10 = jsonObject.getInt("hs10");
        hsSap = jsonObject.getInt("hsSap");
        hsCalculatorMaxBet = jsonObject.getInt("hsCalculatorMaxBet");
    }

    private RoomInfo(){

    }

    public RoomInfo clone(){
        RoomInfo ret = new RoomInfo();
        ret.dealerPlayerId = GameManager.DEALER_UNKNOW;
        ret.curMaxBet = this.minBet;
        ret.comission = this.comission;// He so phe

        ret.maxBet = minBet * _hsMaxBet;
        ret.chickenBet = minBet * _hsChickenBet;

        ret.goldDealer = minBet * _hsGoldDealer;
        ret.goldPlayer = minBet * _hsGoldPlayer;

        ret.minBet = this.minBet;
        ret.maxBet = this.maxBet;

        ret.chickenBet = this.chickenBet;

        ret.goldDealer = this.goldDealer;
        ret.goldPlayer = this.goldPlayer;

        ret.betExtraList = this.betExtraList;

        ret.hs10 = this.hs10;
        ret.hsSap = this.hsSap;
        ret.hsCalculatorMaxBet = this.hsCalculatorMaxBet;
        return ret;
    }

    public RoomInfo createWithBet(int bet){
        RoomInfo ret = new RoomInfo();

        ret.dealerPlayerId = GameManager.DEALER_UNKNOW;
        ret.curMaxBet = bet;

        ret.comission = this.comission;// He so phe


        ret.minBet = bet;// = base
        ret.maxBet = bet * _hsMaxBet;// = base * hsMaxBet -> muc cuoc toi da
        ret.hsCalculatorMaxBet = this.hsCalculatorMaxBet;//he so tinh muc cuoc toi da
        ret.chickenBet = bet * _hsChickenBet;// = base * hsChickenBet -> muc cuoc ga

        ret.goldDealer = bet * _hsGoldDealer;// = base * hsDealer -> So tien de lam chuong
        ret.goldPlayer = bet * _hsGoldPlayer;// = base * hsPlayer -> So tien de vao choi

        ret.betExtraList = new long[_hsExtraBet.length];// = base * hsExtraBet -> Muc Cuoc bien
        for (int i = 0; i < _hsExtraBet.length; i++){
            ret.betExtraList[i] = bet * _hsExtraBet[i];
        }

        ret.hs10 = this.hs10;// He so nhan tien khi 10
        ret.hsSap = this.hsSap;// He so nhan tien khi SAP
        return ret;
    }

    public static long requiredMinGold(long bet){
    	return ServerConstant.roomInfos.get(0)._hsGoldPlayer * bet;
    }
}
