package game.entities;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class TurnType {
    public static final int GAME_START = 1;
    public static final int BET_START = 2;
    public static final int BET_END = 3;
    public static final int GAME_RESULT = 4;
    public static final int GAME_END = 5;
    public static final int CHOICE_DEALER = 6;


    public static final int BOT_JOIN_CHICKEN = 7;
    public static final int BOT_SHOW_CARD = 8;
    public static final int BOT_BET = 9;
}
