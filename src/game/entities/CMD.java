package game.entities;

public class CMD {
    public static final short CUSTOM_LOGIN = 1;

    // Login CMD Group -----------------
    public static final short REQUEST_PLAYER_INFO = 1001;
    public static final short NOTIFY_PLAYER_INFO = 1001;

    public static final short NOTIFY_DAILY_GIFT = 1002;
    public static final short GET_DAILY_GIFT = 1003;
    public static final short GET_CONFIG = 1004;
    public static final short SUPPORT_BEAN = 1005;
    public static final short UPDATE_TOP_USER = 1006;
    public static final short UPDATE_MONEY = 1007;
    // shop
    public static final short SHOP_GOLD = 1008;

    public static final short LOG_MOBILE = 1011;
    public static final short UPDATE_COIN = 1012;
    public static final short RECEIVE_SUPPORT_BEAN = 1013;

    public static final short UPDATE_PLAYER_INFO = 1014;

    public static final short NOTIFY_ONE_TIME_GIFT = 1089;

    public static final short CMD_INFO_INIT_GIFT = 1010;
    public static final short BONUS_LEVEL_UP = 1100;

    public static final short CMD_NEW_MODE = 1500;

    // Channel CMD Group -----------------------------------

    public static final short SELECT_CHANNEL = 2001;
    public static final short EXIT_CHANNEL = 2002;

    // channel
    public static final short UPDATE_LEAF = 2003;
    public static final short CREATE_ROOM = 2004;
    public static final short CHANGE_LEAF = 2005;
    public static final short DELETE_ROOM = 2006;
    public static final short UPDATE_JACKPOT = 2007;
    public static final short REFRESH_TABLE = 2008;
    public static final short ROOM_CREADTED = 2009;
    public static final short LIST_PLAYER_ROOM = 2010;
    public static final short SEARCH_TABLE = 2013;

    public static final short USER_QUICK_PLAY = 2103; //choi nhanh
    public static final short USER_QUICK_PLAY_BY_BET = 2101; // choi nhanh theo muc cuoc.
    public static final short USER_MOBILE_QUICK_PLAY = 2104; // choi nhanh cho mobile.

    public static final short SNAPSHOT = 2500;

    //room - game
    public static final short CMD_SEND_MESSAGE = 3000; //send chat
    public static final short EMOTICON = 3001;
    //public static final short USER_QUICK_PLAY = 3002; //choi nhanh
    public static final short JOIN_ROOM = 3003;
    public static final short NEW_USER_JOIN = 3004;
    public static final short JOIN_ROOM_SUCCESS = 3005;
    public static final short QUICK_PLAY_FAIL = 3006;
    public static final short JOIN_ROOM_FAIL = 3007;
    public static final short LEAVE_ROOM = 3008;
    public static final short LEAVE_ROOM_SUCCESS = 3009;
    public static final short NOTIFY_KICK_FROM_ROOM = 3010;
    public static final short UPDATE_OWNER_ROOM = 3011;

    //game
    public static final short CMD_AUTOSTART = 3012;
    public static final short REQUEST_READY = 3013;
    public static final short NOTIFY_GAME_START = 3014;
    public static final short NOTIFY_GAME_END = 3015; //ket thuc game
    public static final short REQUEST_END_CARDS = 3018;
    public static final short XAP_BAI = 3020;
    public static final short NOTIFY_USER_GET_JACKPOT = 3027; //send msg cho tat ca user trong kenh
    public static final short RECEIVE_USER_REQ_QUIT_ROOM = 3029; //dang ky thoat phong
    public static final short NOTIFY_USER_REQ_QUIT_ROOM = 3029; //notify dang ky thoat game
    public static final short RECEIVE_USER_NOT_REQ_QUIT_ROOM = 3030; //huy dang ky thoat phong
    public static final short NOTIFY_USER_NOT_REQ_QUIT_ROOM = 3030; //notify huy dang ky thoat game
    public static final short CMD_IS_PLAYING = 3201;
    //msg mode 3cay
    public static final short INVITE_BET_EXTRA = 3300;//Danh bien
    public static final short PLAYER_SHOW_CARD = 3301;
    public static final short PLAYER_JOIN_CHICKEN = 3302;
    public static final short BET = 3303;
    public static final short PLAYER_CARD = 3304;
    public static final short BET_START = 3305;
    public static final short UPDATE_PLAYER_DATA = 3306;
    public static final short REPONSE_INVITE_BET_EXTRA = 3307;
    public static final short BET_EXTRA_RESULT = 3308;
    public static final short ROOM_INFO = 3309;
    public static final short THI_CHUONG_RESULT = 3310;
    public static final short NOTIFY_CHANGE_DEALER = 3311;
    public static final short NOTY_GAME_RESULT = 3312;
    public static final short BET_ERROR = 3313;

    //end game

    public static final short REQUEST_VIEW_INFO = 3100; //dung rieng cho nguoi xem game
    public static final short NOTIFY_VIEW_INFO = 3101; //dung rieng cho nguoi xem game
    public static final short NOTIFY_VIEWERTOUSER = 3104; //dung rieng cho nguoi xem game

    public static final short USER_NOTIFY_CONNECT = 3107; //kiem tra xem nguoi choi con connect hay ko
    public static final short USER_MAXIMIZE = 3108; //log maximize
    public static final short USER_IDLE_60S = 3109; //msg thoat phong do 60s ko lam gi ca
    //public static final short VIEWER_REPLY = 3108; //kiem tra thang viewgame con trong phong hay khong - hoac no co bi disconect ko?

    public static final short REQ_LIST_PLAYER = 3200;
    public static final short INVITATION = 3201;
    public static final short RECEIVE_INVITATION = 3202;
    public static final short ACCEPT_INVITATION = 3203;
    public static final short CANCEL_INVITATION = 3204;

    public static final short NOTIFY_INFO_TABLE_RECONNECT = 3900; //thong tin ban choi khi reconect

    public static final short CMD_CHECK_CODE = 4900;
    public static final short CMD_RESPONSE_CHECK_CODE = 4901;
    public static final short CMD_ENABLE_EVENT = 4902; //event cam gioi
    public static final short CMD_REQUEST_GIFT_CODE = 4903; //event cam gioi
    public static final short CMD_NOTIFY_GIFT_CODE = 4904; //event cam gioi

    public static final short NAP_THE_MOBILE = 4888;

    //item
    public static final short USE_ITEMS = 8000;
    public static final short NEW_ITEM = 8001;
    public static final short BUY_ITEM = 8002;
    public static final short MY_ITEM = 8003;
    public static final short USE_TUONG_TAC = 8004;
    public static final short DELETE_ITEM = 8005;
    public static final short CHEAT_LOCK = 8006;
    public static final short ACTIVE_ITEM_LOCK = 8007;
    public static final short BONUS_ITEM_LOCK = 8008;

    public static final short VIP_REGISTER = 6001;
    public static final short VIP_INFO = 6002;
    public static final short VIP_CHECK_TIME = 6003;
    public static final short VIP_OUT_OF_TIME = 6004;
    public static final short VIP_CHEAT = 6005;

    //minigame
    public static final short MNG_SCRATCHCARD_GAME = 7000;
    public static final short MNG_ID_SCRATCH_CARD = 7001;
    public static final short MNG_FINISH_SCRATCH_CARD_GAME = 7002;
    public static final short MNG_SCRATCH_ALL_CARD = 7003;
    public static final short MNG_BUY_SCRATCH_CARD = 7004;
    //mobile
    public static final short MNG_MOBILE_SCRATCH_ALL_CARD = 7005;
    public static final short MNG_MOBILE_FINISH = 7006;
    public static final short MNG_MOBILE_CONFIG = 7007;

    //test
    public static final short CONFIG_CARDS = 9000;
    public static final short CHEAT_MONEY = 9001;
    public static final short CHEAT_JACKPOT = 9002;
    public static final short CHEAT_EXP = 9003;
    public static final short CHEAT_MONEY2 = 9004;
    public static final short CHEAT_MISSION_CAM_GIOI = 9005;
    public static final short CHEAT_RESET_MISSION_CAM_GIOI = 9006;
    //test

    public static final short LOCK_ACCOUNT = 68;

    public static final short UPDATE_G = 500;
    public static final short USER_MOBILE = 600; //goi tin nguoi dung mobile gui len
    public static final short ECHO = 1000; //goi tin nguoi dung mobile gui len


}
