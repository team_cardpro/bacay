package game.modules.gameRoom.cmd;


import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.common.business.Debug;
import game.entities.CMD;
import game.entities.PlayerInfo;
import game.entities.ServerConstant;

import java.nio.ByteBuffer;
import java.util.Arrays;


public class SendJoinRoomSuccess extends BaseMsg
{
    public byte uChair;
    public byte comission;
    public byte comissionJackpot;
    public byte betType;
    public long roomBet;
    public byte roomOwner;
    public int roomIndex;
    public int roomId;
    public byte roomType;

    public byte playerStatus[] = new byte[ServerConstant.MAX_PLAYER];
    public PlayerInfo playerList[] = new PlayerInfo[ServerConstant.MAX_PLAYER];
    public long goldTemp[] = new long[ServerConstant.MAX_PLAYER];
    public int roomOwnerId;
    public boolean roomLock;
    public byte cuoclon;
    public byte vip[] = new byte[ServerConstant.MAX_PLAYER];

    public SendJoinRoomSuccess()
    {
        super(CMD.JOIN_ROOM_SUCCESS);
    }

    @Override
    public byte[] createData()
    {
        ByteBuffer bf = makeBuffer();
        bf.put(uChair);
        bf.put(comission);
        bf.put(comissionJackpot);
        bf.put(betType);
        bf.putLong(roomBet);
        bf.put(roomOwner);
        bf.putInt(roomIndex);
        bf.putInt(roomId);
        bf.put(roomType);
        bf.putShort((short) playerStatus.length);
        for (int i=0; i < playerStatus.length; i++)
        {
            bf.put(playerStatus[i]);
        }

        bf.putShort((short) playerList.length);
        for (int i = 0; i < playerList.length; i++)
        {
            if (playerList[i] == null) playerList[i] = new PlayerInfo();
            putStr(bf, playerList[i].avtURL);
            bf.putInt(playerList[i].getUId());
            putStr(bf, playerList[i].displayName);
            bf.putLong(playerList[i].bean + goldTemp[i]);
            bf.putLong(0);
            bf.putInt(playerList[i].winCount);
            bf.putInt(playerList[i].lostCount);
            bf.putInt(playerList[i].avatarId);
            putStr(bf, "127.0.0.1");
        }
        bf.putInt( roomOwnerId );
        putBoolean( bf , roomLock );
        bf.put( cuoclon );
        bf.putShort((short) vip.length);
        for (int i=0; i< vip.length; i++)
        {
            bf.put(vip[i]);
        }

        System.err.println(this.toString());

        return packBuffer(bf);
    }

    @Override
    public String toString() {
        return "SendJoinRoomSuccess [uChair=" + uChair + ", comission=" + comission + ", comissionJackpot="
                + comissionJackpot + ", betType=" + betType + ", roomBet=" + roomBet + ", roomOwner=" + roomOwner
                + ", roomIndex=" + roomIndex + ", roomId=" + roomId + ", roomType=" + roomType + ", playerStatus="
                + Arrays.toString(playerStatus) + ", playerList=" + Arrays.toString(playerList) + ", roomOwnerId="
                + roomOwnerId + ", roomLock=" + roomLock + ", cuoclon=" + cuoclon + ", vip=" + Arrays.toString(vip)
                + "]";
    }


}
