package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import game.entities.RoomInfo;
import game.entities.ServerConstant;
import game.modules.bot.BotManager;
import game.modules.channel.ChannelModule;
import game.server.GameManager;
import game.utils.CacheConfig;

public class BaCayExtension extends GameExtension {
	public BaCayExtension() {
		super();
		initGameConfig();
		GameExtension.mapReloadConfig.put("MaxBetEnableBot", 1);
		GameExtension.mapReloadConfig.put("WIN_RATE", 0);

	}
	public void reloadConfigSuccess(){
		super.reloadConfigSuccess();
		BotManager.MaxBetEnableBot = CacheConfig.instance().getInt("MaxBetEnableBot", 1);
		GameManager.WIN_RATE = CacheConfig.instance().getInt("WIN_RATE", 0);
		Debug.trace(String.format("BotManager.MaxBetEnableBot:%s;"
				+ "GameManager.WIN_RATE:%s", 
				BotManager.MaxBetEnableBot,
				GameManager.WIN_RATE
				));
	}
	private void initGameConfig() {
		String path = System.getProperty("user.dir");
		File file = new File(path + "/conf/configBaCay.json");
		StringBuffer contents = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String text = null;
			while ((text = reader.readLine()) != null) {
				contents.append(text).append(System.getProperty("line.separator"));
			}

			JSONObject rootObj = new JSONObject(contents.toString());
			JSONArray roomObj = rootObj.getJSONArray("room");
			for (int i = 0; i < roomObj.length(); i++) {
				ServerConstant.roomInfos.add(new RoomInfo(roomObj.getJSONObject(i)));
			}

		} catch (FileNotFoundException e) {
			CommonHandle.writeErrLog(e);
		} catch (IOException e) {
			CommonHandle.writeErrLog(e);
		} catch (JSONException e) {
			e.printStackTrace();
			CommonHandle.writeErrLog(e);
		}
	}

}
