package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;
import game.entities.ServerConstant;

import java.nio.ByteBuffer;

public class SendNotifyViewerToUser extends BaseMsg {
    public int State[] = new int[ServerConstant.MAX_PLAYER];

    public SendNotifyViewerToUser() {
        super(CMD.NOTIFY_VIEWERTOUSER);
    }

    public SendNotifyViewerToUser(short s, int i) {
        super(s, i);
    }

    public SendNotifyViewerToUser(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        putIntArray(bf, State);
        return packBuffer(bf);
    }
}
