package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendNotifyUsergetJackpot extends BaseMsg {
    public String username = "";
    public long jackpot;
    public int roomID;
    public int userID;

    public SendNotifyUsergetJackpot() {
        super(CMD.NOTIFY_USER_GET_JACKPOT);
    }

    public SendNotifyUsergetJackpot(short s, int i) {
        super(s, i);
    }

    public SendNotifyUsergetJackpot(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        putStr(bf, username);
        bf.putLong(jackpot);
        bf.putInt(roomID);
        bf.putInt(userID);
        return packBuffer(bf);
    }
}
