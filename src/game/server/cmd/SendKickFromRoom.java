package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendKickFromRoom extends BaseMsg {
    public class Reason {
    	public static final int eKICK_REASON_DEFAULT = -2;
        public static final int eKICK_REASON_RESTART_SERVER = -1;
        public static final int eKICK_REASON_MONEY = 0;
        public static final int eKICK_REASON_READY = 1;
        // kick ra neu co ng quit -> room chi con 1 ng ko ready
        public static final int eKICK_REASON_LAST_USER = 2;
        public static final int eKICK_REASON_SYSTEM_ERROR = 3;
        public static final int eKICK_REASON_GAME_ERROR = 4;
        public static final int eKICK_REASON_G = 5;
        public static final int eKICK_REASON_NO_PLAY = 6;
        public static final int eKICK_REASON_ROOM_FULL = 7;
        public static final int eKICK_NO_DEALER = 8;
    }

    public int chair;
    public int reason;
    public int uId;

    public SendKickFromRoom() {
        super(CMD.NOTIFY_KICK_FROM_ROOM);
        reason = Reason.eKICK_REASON_DEFAULT;
    }

    public SendKickFromRoom(short s, int i) {
        super(s, i);
    }

    public SendKickFromRoom(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put((byte) chair);
        bf.put((byte) reason);
        bf.putInt(uId);
        return packBuffer(bf);
    }
}
