package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.*;

import java.nio.ByteBuffer;
import java.util.List;

public class SendInfotableReconnect extends BaseMsg {


    public byte dealer;

    public class Reason {
        public static final int END_GAME = 0;
        public static final int PLAYING = 1;
    }

    public SendInfotableReconnect() {
        super(CMD.NOTIFY_INFO_TABLE_RECONNECT);
    }


    public byte playerStatus[] = new byte[ServerConstant.MAX_PLAYER];
    public PlayerInfo[] players = new PlayerInfo[ServerConstant.MAX_PLAYER];
    public long[] goldTemp = new long[ServerConstant.MAX_PLAYER];
    public byte vip[] = new byte[ServerConstant.MAX_PLAYER];
    public byte currentTime;
    public long roomBet;
    public int roomId;
    public byte gameState;
    public byte comission;
    public byte roomOwner;//chair cua chu phong
    public byte roomType;
    public byte myChair;

    public long[] betExtra;
    public List<DeckInfo> deckInfos;
    public byte turnType;
    public int turnTime;
    public long goldInChicken;
    public byte[] myCard;
    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        //status
        putByteArray(bf, playerStatus);
        //players
        bf.putShort((short) players.length);
        for (int i = 0; i < players.length; i++) {
            if (players[i] == null)
                players[i] = new PlayerInfo();
            putStr(bf, players[i].avtURL);
            bf.putInt(players[i].getUId());
            putStr(bf, players[i].displayName);
            bf.putLong(players[i].bean + goldTemp[i]);
            bf.putLong(0);
            bf.putInt(players[i].winCount);
            bf.putInt(players[i].lostCount);
            bf.putInt(players[i].avatarId);
            putStr(bf, players[i].ip);
        }

        //roomInfo
        bf.put(currentTime);
        putLong(bf, roomBet);
        bf.putInt(roomId);
        bf.put(gameState);
        bf.put(comission);
        bf.put(roomOwner);
        bf.put(roomType);
        bf.put(myChair);
        bf.put(dealer);


        bf.put(turnType);
        bf.putInt(turnTime);

        bf.putLong(goldInChicken);
        //Card gameplayer
        bf.put((byte) deckInfos.size());
        for (int i = 0; i < deckInfos.size(); i++)
        {
            DeckInfo deckInfo = deckInfos.get(i);
            bf.put(deckInfo.chair);

            //show card
            putBoolean(bf, deckInfo.isShowCard);
            if (deckInfo.isShowCard){
                putByteArray(bf, deckInfo.cardIds);
            }

            //cuoc voi chuong
            putBoolean(bf, deckInfo.isBet);
            if (deckInfo.isBet){
                putLong(bf, deckInfo.goldBet);
            }

            //danh ga
            putBoolean(bf, deckInfo.isChickedJoined);

            //cuoc bien
            bf.put(deckInfo.betExtraInfo.status);
            bf.putInt(deckInfo.betExtraInfo.betIdx);
        }

        putByteArray(bf, myCard);


        bf.put((byte) betExtra.length);
        for (int i = 0; i < betExtra.length; i++){
            putLong(bf, betExtra[i]);
        }

        return packBuffer(bf);
    }
}
