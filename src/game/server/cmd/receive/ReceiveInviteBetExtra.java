package game.server.cmd.receive;

import bitzero.server.extensions.data.BaseCmd;
import bitzero.server.extensions.data.DataCmd;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class ReceiveInviteBetExtra extends BaseCmd {
    public int betIdx;
    public byte chair;

    public ReceiveInviteBetExtra(DataCmd data) {
        super(data);
        unpackData();
    }

    @Override
    public void unpackData() {
        ByteBuffer bf = makeBuffer();
        betIdx = readInt(bf);
        chair = readByte(bf);
    }
}
