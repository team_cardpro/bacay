package game.server.cmd.receive;

import bitzero.server.extensions.data.BaseCmd;
import bitzero.server.extensions.data.DataCmd;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class ReceiveBet extends BaseCmd {
    public long gold;

    public ReceiveBet(DataCmd data) {
        super(data);
        unpackData();
    }

    @Override
    public void unpackData() {
        ByteBuffer bf = makeBuffer();
        gold = bf.getLong();
    }
}
