package game.server.cmd.receive;

import bitzero.server.extensions.data.BaseCmd;
import bitzero.server.extensions.data.DataCmd;

import java.nio.ByteBuffer;


/**
 * Created by mrtun9z on 3/29/16.
 */
public class ReceiveResponseInviteBetExtra extends BaseCmd {
    public boolean isAccept;
    public int chair;

    public ReceiveResponseInviteBetExtra(DataCmd data) {
        super(data);
        unpackData();
    }

    @Override
    public void unpackData() {
        ByteBuffer bf = makeBuffer();
        isAccept = readBoolean(bf);
        chair = readInt(bf);
    }
}
