package game.server.cmd;

import bitzero.server.extensions.data.BaseCmd;
import bitzero.server.extensions.data.DataCmd;

import java.nio.ByteBuffer;

public class ReceiveMaximize extends BaseCmd {
    public boolean maximize;

    public ReceiveMaximize(DataCmd dataCmd) {
        super(dataCmd);
    }

    @Override
    public void unpackData() {
        ByteBuffer bf = makeBuffer();
        maximize = readBoolean(bf);
    }
}
