package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendNotifyNotReqQuitRoom extends BaseMsg {
    public static final short REQ_SUCCESS = 0;
    public static final short REQ_FAIL = 1;
    public int chair;
    public int reason;

    public SendNotifyNotReqQuitRoom() {
        super(CMD.NOTIFY_USER_NOT_REQ_QUIT_ROOM);
    }

    public SendNotifyNotReqQuitRoom(short s, int i) {
        super(s, i);
    }

    public SendNotifyNotReqQuitRoom(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put((byte) chair);
        bf.put((byte) reason);
        return packBuffer(bf);
    }
}
