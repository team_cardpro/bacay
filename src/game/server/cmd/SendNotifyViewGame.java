package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;
import game.entities.DeckInfo;
import game.entities.ServerConstant;
import game.server.GameManager;
import game.server.GameServer;

import java.nio.ByteBuffer;
import java.util.List;

public class SendNotifyViewGame extends BaseMsg {
    public int gameState;
    public boolean[] isPlaying = new boolean[ServerConstant.MAX_PLAYER];

    //Tung
    public List<DeckInfo> deckInfos;
    public int goldInChicken;
    public int turnTime;
    public byte turnType;

    public SendNotifyViewGame() {
        super(CMD.NOTIFY_VIEW_INFO);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put((byte) gameState);

        if (gameState == GameServer.gsPlay){
            putBooleanArray(bf, isPlaying);


            //Tung
            bf.put((byte) deckInfos.size());
            for (int i = 0; i < deckInfos.size(); i++)
            {
                DeckInfo deckInfo = deckInfos.get(i);
                bf.put(deckInfo.chair);

                //show card
                putBoolean(bf, deckInfo.isShowCard);
                if (deckInfo.isShowCard){
                    putByteArray(bf, deckInfo.cardIds);
                }

                //cuoc voi chuong
                putBoolean(bf, deckInfo.isBet);
                if (deckInfo.isBet){
                    putLong(bf, deckInfo.goldBet);
                }

                //danh ga
                putBoolean(bf, deckInfo.isChickedJoined);

                //cuoc bien
                bf.put(deckInfo.betExtraInfo.status);
                bf.putInt(deckInfo.betExtraInfo.betIdx);
            }
        }



        bf.putInt(goldInChicken);
        bf.putInt(turnTime);
        bf.put(turnType);

        return packBuffer(bf);
    }
}
