package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendRequestEndCard extends BaseMsg {
    public SendRequestEndCard() {
        super(CMD.REQUEST_END_CARDS);
    }

    public SendRequestEndCard(short s, int i) {
        super(s, i);
    }

    public SendRequestEndCard(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        return packBuffer(bf);
    }
}
