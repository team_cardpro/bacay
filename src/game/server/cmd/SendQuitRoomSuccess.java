package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendQuitRoomSuccess extends BaseMsg {
    public SendQuitRoomSuccess() {
        super(CMD.LEAVE_ROOM_SUCCESS);
    }

    public SendQuitRoomSuccess(short s, int i) {
        super(s, i);
    }

    public SendQuitRoomSuccess(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        return packBuffer(bf);
    }
}
