package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendUpdateOwnerRoom extends BaseMsg {
    public int roomOwner;

    public SendUpdateOwnerRoom() {
        super(CMD.UPDATE_OWNER_ROOM);
    }

    public SendUpdateOwnerRoom(short s, int i) {
        super(s, i);
    }

    public SendUpdateOwnerRoom(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put((byte) roomOwner);
        return packBuffer(bf);
    }
}
