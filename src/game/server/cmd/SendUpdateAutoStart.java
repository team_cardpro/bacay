package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

import java.nio.ByteBuffer;

public class SendUpdateAutoStart extends BaseMsg {
    public boolean isAutoStart;
    public int autoStartTime;

    public SendUpdateAutoStart() {
        super(CMD.CMD_AUTOSTART);
    }

    public SendUpdateAutoStart(short s, int i) {
        super(s, i);
    }

    public SendUpdateAutoStart(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();


        putBoolean(bf, isAutoStart);
        bf.putInt(autoStartTime);
        return packBuffer(bf);
    }
}
