package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;

public class SendUserIdle extends BaseMsg {
    public SendUserIdle() {
        super(CMD.USER_IDLE_60S);
    }

    public SendUserIdle(short s, int i) {
        super(s, i);
    }

    public SendUserIdle(short s) {
        super(s);
    }
}
