package game.server.cmd;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;
import game.entities.ServerConstant;

import java.nio.ByteBuffer;

public class SendXapBai extends BaseMsg {
    public long[] Money = new long[ServerConstant.MAX_PLAYER];
    public int playerID;

    public SendXapBai() {
        super(CMD.XAP_BAI);
    }

    public SendXapBai(short s, int i) {
        super(s, i);
    }

    public SendXapBai(short s) {
        super(s);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        //putLongArray(bf, Money);
        bf.putShort((short) Money.length);
        for (int i = 0; i < Money.length; i++) {
            bf.putLong(Money[i]);
        }
        bf.putInt(playerID);
        return packBuffer(bf);
    }
}
