package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendPlayerJoinChicken extends BaseMsg {
    public byte chair;
    public long gold;
    public SendPlayerJoinChicken() {
        super(CMD.PLAYER_JOIN_CHICKEN);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(chair);
        bf.putLong(gold);
        return packBuffer(bf);
    }
}
