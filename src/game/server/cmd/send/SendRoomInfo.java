package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.common.business.Debug;
import game.entities.CMD;
import game.entities.RoomInfo;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/30/16.
 */
public class SendRoomInfo extends BaseMsg {
    public RoomInfo roomInfo;

    public SendRoomInfo() {
        super(CMD.ROOM_INFO);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.putInt(roomInfo.maxBet);
        bf.putInt(roomInfo.minBet);
        bf.putInt(roomInfo.curMaxBet);
        bf.putInt(roomInfo.chickenBet);
        bf.put((byte) (roomInfo.dealerPlayerId - 1));
        bf.put((byte) roomInfo.comission);
        bf.put((byte) roomInfo.hs10);
        bf.put((byte) roomInfo.hsSap);
        bf.put((byte) roomInfo.betExtraList.length);
        for (int i = 0; i < roomInfo.betExtraList.length; i++){
            bf.putLong(roomInfo.betExtraList[i]);
        }
        return packBuffer(bf);
    }
}
