package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;
import game.entities.GameResult;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by mrtun9z on 4/9/16.
 */

public class SendNotifyGameResult extends BaseMsg {


    public SendNotifyGameResult() {
        super(CMD.NOTY_GAME_RESULT);
    }

    public byte chairChicken; //Ghe an ga
    public long goldChicken;//So tien an duoc trong ga

    public byte dealer;
    public long goldDealer;
    public byte statusDealer;//0: thang, 1: thua, 2: phat luong, 3: ca lang sang tien
    public int scoreDealer;
    public byte factorDealer;

    static public final byte DEALER_WIN = 0;
    static public final byte DEALER_LOSE = 1;
    static public final byte DEALER_PHAT_LUONG = 2;
    static public final byte DEALER_CA_LANG_SANG_TIEN = 3;
    static public final byte DEALER_HOA = 4;

    public List<GameResult> gameResults;

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();

        bf.put(chairChicken);
        bf.putLong(goldChicken);

        bf.put(dealer);
        bf.putLong(goldDealer);
        bf.put(statusDealer);
        bf.putInt(scoreDealer);
        bf.put(factorDealer);

        bf.put((byte) gameResults.size());
        for (int i = 0; i < gameResults.size(); i++)
        {
            bf.put((byte) gameResults.get(i).chair);
            putBoolean(bf, gameResults.get(i).isWin);
            bf.putLong(gameResults.get(i).gold);
            bf.putInt(gameResults.get(i).score);
            bf.put(gameResults.get(i).factor);
        }

        return packBuffer(bf);
    }
}

