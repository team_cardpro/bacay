package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendInviteBetExtra extends BaseMsg {
    public int betIdx;
    public byte chair;

    public SendInviteBetExtra() {
        super(CMD.INVITE_BET_EXTRA);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.putInt(betIdx);
        bf.put(chair);
        return packBuffer(bf);
    }
}
