package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.common.business.Debug;
import game.entities.CMD;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by mrtun9z on 4/1/16.
 */
public class SendThiChuongResult extends BaseMsg {
    public byte[] chair;
    public byte[] cardId;
    public byte dealerChair;
    public boolean[] isPlaying;

    public SendThiChuongResult() {
        super(CMD.THI_CHUONG_RESULT);
    }



    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(dealerChair);
        bf.put((byte) chair.length);

        Debug.trace("SendThiChuongResult::createData: " + dealerChair + " length " + chair.length);
        for (int i = 0; i < chair.length; i++){
            bf.put(chair[i]);
            bf.put(cardId[i]);
        }

        putBooleanArray(bf, isPlaying);
        return packBuffer(bf);
    }
}
