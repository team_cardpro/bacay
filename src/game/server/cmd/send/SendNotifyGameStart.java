package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;

import game.entities.CMD;
import game.entities.ServerConstant;

import java.nio.ByteBuffer;

public class SendNotifyGameStart extends BaseMsg {
    public int nGameCount;
    public byte dealer;
    public int gameTime;
    public int curMaxBet;
    public boolean[] isPlaying = new boolean[ServerConstant.MAX_PLAYER];

    public SendNotifyGameStart() {
        super(CMD.NOTIFY_GAME_START);
    }


    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.putInt(dealer);
        bf.putInt(nGameCount);
        bf.putInt(gameTime);
        putBooleanArray(bf, isPlaying);
        bf.putInt(curMaxBet);

        return packBuffer(bf);
    }
}
