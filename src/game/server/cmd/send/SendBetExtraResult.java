package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.common.business.Debug;
import game.entities.CMD;
import game.entities.GameResult;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by mrtun9z on 3/29/16.
 */
public class SendBetExtraResult extends BaseMsg{
    public List<GameResult> gameResults;

    public SendBetExtraResult() {
        super(CMD.BET_EXTRA_RESULT);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put((byte) gameResults.size());
        for (int i = 0; i < gameResults.size(); i++)
        {
            GameResult gameResult = gameResults.get(i);
            bf.putInt(gameResult.chair);
            bf.putInt((int) gameResult.gold);
            putBoolean(bf, gameResult.isWin);
            bf.put(gameResult.factor);
        }

        return packBuffer(bf);
    }
}
