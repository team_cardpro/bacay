package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendPlayerShowCard extends BaseMsg {
    public byte[] cardId;
    public byte chair;
    public SendPlayerShowCard() {
        super(CMD.PLAYER_SHOW_CARD);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(chair);
        putByteArray(bf, cardId);

        return packBuffer(bf);
    }
}
