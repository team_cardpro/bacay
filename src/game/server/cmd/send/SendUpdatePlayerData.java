package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;
import game.server.GamePlayer;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/28/16.
 */
public class SendUpdatePlayerData extends BaseMsg {
    public byte chair;
    public long gold;
    public SendUpdatePlayerData() {
        super(CMD.UPDATE_PLAYER_DATA);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();

        bf.put(chair);
        bf.putLong(gold);

        return packBuffer(bf);
    }
}
