package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 4/13/16.
 */
public class SendNotifyBetError extends BaseMsg {
    static public final byte BET_WITH_DEALER = 1;
    static public final byte BET_CHICKEN = 2;
    static public final byte BET_WITH_ANOTHER_PLAYER = 3;

    static public final byte REASON_NOT_MONEY = 1;
    static public final byte REASON_OPP_NOT_MONEY = 2;

    public byte type;
    public byte reason;

    public SendNotifyBetError() {
        super(CMD.BET_ERROR);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(type);
        bf.put(reason);
        return packBuffer(bf);
    }
}
