package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.common.business.Debug;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendBet extends BaseMsg {
    public long gold;
    public byte chair;

    public SendBet() {
        super(CMD.BET);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.putLong(gold);
        bf.put(chair);
        Debug.trace("SendBet::createData: " + chair);
        return packBuffer(bf);
    }
}
