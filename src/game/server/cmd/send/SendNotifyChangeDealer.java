package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendNotifyChangeDealer extends BaseMsg {
    public byte chair;

    public SendNotifyChangeDealer() {
        super(CMD.NOTIFY_CHANGE_DEALER);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(chair);

        return packBuffer(bf);
    }
}
