package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/29/16.
 */
public class SendResponseInviteBetExtra extends BaseMsg {
    public byte chair;
    public boolean isAccept;
    public byte betIdx;
    public SendResponseInviteBetExtra() {
        super(CMD.REPONSE_INVITE_BET_EXTRA);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.put(chair);
        putBoolean(bf, isAccept);
        if (isAccept){
            bf.put(betIdx);
        }
        return packBuffer(bf);
    }
}
