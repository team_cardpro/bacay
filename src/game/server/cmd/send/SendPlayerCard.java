package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by mrtun9z on 3/24/16.
 */
public class SendPlayerCard extends BaseMsg {
    public byte[] cardIds;
    public int time;
    public SendPlayerCard() {
        super(CMD.PLAYER_CARD);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        putByteArray(bf, cardIds);
        bf.putInt(time);
        return packBuffer(bf);
    }

	@Override
	public String toString() {
		return "SendPlayerCard [cardIds=" + Arrays.toString(cardIds) + ", time=" + time + "]";
	}
    
}
