package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

/**
 * Created by mrtun9z on 4/9/16.
 */
public class SendNotifyGameEnd extends BaseMsg {


    public SendNotifyGameEnd() {
        super(CMD.NOTIFY_GAME_END);
    }


}
