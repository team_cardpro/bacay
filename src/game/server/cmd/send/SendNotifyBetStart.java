package game.server.cmd.send;

import bitzero.server.extensions.data.BaseMsg;
import game.entities.CMD;

import java.nio.ByteBuffer;

/**
 * Created by mrtun9z on 3/25/16.
 */
public class SendNotifyBetStart extends BaseMsg {
    public int turnTime;
    public SendNotifyBetStart() {
        super(CMD.BET_START);
    }

    @Override
    public byte[] createData() {
        ByteBuffer bf = makeBuffer();
        bf.putInt(turnTime);
        return packBuffer(bf);
    }
}
