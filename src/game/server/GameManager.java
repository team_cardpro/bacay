package game.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import game.modules.BaseEvent.hu.HuConstant;
import game.modules.bot.BotManager;
import game.server.BaCayLogic.PlayerCard;
import game.server.cmd.*;
import game.server.cmd.SendKickFromRoom;
import game.server.cmd.SendNotifyViewGame;
import game.server.cmd.SendUpdateAutoStart;
import game.server.cmd.send.*;
import net.spy.memcached.compat.log.Logger;
import org.slf4j.LoggerFactory;

import bitzero.server.entities.Room;
import bitzero.server.entities.User;
import bitzero.server.entities.Zone;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import bitzero.util.datacontroller.business.DataControllerException;
import game.BaCayExtension;
import game.channel.bo.GameChannel;
import game.entities.BetExtraInfo;
import game.entities.DeckInfo;
import game.entities.GameResult;
import game.entities.PlayerInfo;
import game.entities.RoomInfo;
import game.entities.ServerConstant;
import game.entities.TurnTime;
import game.entities.TurnType;
import game.modules.gameCheat.cmd.ReceiveConfigCards;
import game.server.BaCayLogic.Card;
import game.server.BaCayLogic.PlayerCard;
import game.server.cmd.ReceiveMaximize;
import game.server.cmd.SendInfotableReconnect;
import game.server.cmd.SendKickFromRoom;
import game.server.cmd.SendNotifyNotReqQuitRoom;
import game.server.cmd.SendNotifyReqQuitRoom;
import game.server.cmd.SendNotifyViewGame;
import game.server.cmd.SendUpdateAutoStart;
import game.server.cmd.receive.ReceivePlayerReady;
import game.server.cmd.send.SendBetExtraResult;
import game.server.cmd.send.SendNotifyBetStart;
import game.server.cmd.send.SendNotifyChangeDealer;
import game.server.cmd.send.SendNotifyGameEnd;
import game.server.cmd.send.SendNotifyGameResult;
import game.server.cmd.send.SendNotifyGameStart;
import game.server.cmd.send.SendPlayerCard;
import game.server.cmd.send.SendPlayerShowCard;
import game.server.cmd.send.SendRoomInfo;
import game.server.cmd.send.SendThiChuongResult;
import game.server.cmd.send.SendUpdatePlayerData;
import game.utils.MetricLog;
import game.utils.Utils;

public class GameManager {
	public static final int gmNoPlay = 0;
	public static final int gmPlay = 1;
	public static final int gmEndGame = 2;

	public static int WIN_RATE = 0;

	private final org.slf4j.Logger logger = LoggerFactory.getLogger("BaCayLogic");

	private long minGold;
	public int nViewer = 0;
	private int roomType = 0;
	// tala
	// private volatile int comission = 0;
	// private volatile int comissionJackpot = 0;
	private volatile int betType = ServerConstant.BET_TYPE_BEAN;

	private GameServer gameServer;
	private int roomOwnerID = -1;
	private volatile int betIndex = 0;
	private volatile long moneyBet = 100;
	private long timeStart; // metric log thoi gian 1 game
	private volatile boolean isAutoStartGame = false;
	// private volatile int autoStartCountDown = 0;
	private volatile int gameTime;
	// private volatile int gameState;
	private volatile int timeStep;
	private volatile int stepEndGame = -1;
	private volatile int[] cardID = null;

	private volatile boolean cuoclon;
	// private Random rnd;
	private volatile int GameCount;
	private volatile boolean isConfigCard = false;
	private final int[] ConfigCards = new int[ServerConstant.MAX_CARDS]; // luu
																			// lai
																			// id
																			// quan
																			// bai
																			// ->dung
																			// de
																			// cheat
	// private volatile int[] dsSoBai = new int[ServerConstant.MAX_PLAYER + 1];
	// // thu
	// tu
	// so
	// bai

	// public volatile int cheat = 0;

	// Tung

	public static final int DEALER_UNKNOW = -1; // Chuong thoat khoi phong
	public static final int DEALER_ENOUGHT_MONEY = -2;// Chuong cu ko du tien
	// public volatile int dealerChair = 0; //Ghe cua thang chuong

	public RoomInfo roomInfo = null;

	static private final Map<Integer, Integer> mapTurnType = new HashMap<Integer, Integer>();
	private volatile int timeCountDown;
	private volatile int curTurnType;
	private boolean needThiChuong = false;
	public String codeVanChoi = "";

	// end tung

	public void trace(Object...objects) {
		String msg = this.getClass().getSimpleName() + "::" + gameServer.getRoomId() + "==>";
		for (Object object : objects) {
			msg += object + " ";
		}
		Debug.trace(msg);
	}

	public GameManager() {
		super();

		if (mapTurnType.isEmpty()) {
			mapTurnType.put(TurnType.BET_START, TurnTime.TIME_TO_BET_END);
			mapTurnType.put(TurnType.BET_END, TurnTime.TIME_TO_GAME_RESULT);
			mapTurnType.put(TurnType.GAME_RESULT, TurnTime.TIME_TO_GAME_END);
			mapTurnType.put(TurnType.GAME_END, TurnTime.TIME_TO_NEW_GAME);
			mapTurnType.put(TurnType.GAME_START, TurnTime.TIME_TO_BET_START);
			mapTurnType.put(TurnType.CHOICE_DEALER, TurnTime.CHOICE_DEALER);
		}

		roomOwnerID = -1;
		// IsFreeBeanRoom = false;
		// setGameState(gmNoPlay);
	}

	public void choiceDealer() {
		if (gameServer.getPlayerReadyList().size() == 0) {
			gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_NO_DEALER);
			Debug.trace("GameManager::choiceDealer: " + gameServer.getRoom().getId());
			return;
		}

		if (roomInfo.dealerPlayerId == DEALER_UNKNOW || roomInfo.dealerPlayerId == DEALER_ENOUGHT_MONEY) {
			// Chua co ai lam chuong
			needThiChuong = true;

		} else {
			// Kiem tra chuong hien tai du dieu kien ko
			if (!checkMoneyOfDealerPosition(roomInfo.dealerPlayerId)) {
				roomInfo.dealerPlayerId = DEALER_ENOUGHT_MONEY;
				choiceDealer();
			} else {
				setDealer(roomInfo.dealerPlayerId);
			}
		}
	}

	private boolean doThiChuong() {
		synchronized (this) {
			Debug.info("GameManager::doThiChuong room " + gameServer.getRoom().getId() + ":  START " + timeCountDown);
			needThiChuong = false;
			gameServer.setServerState(GameServer.gsPlay);
			// setGameState(gmPlay);
			List<GamePlayer> dealerList = new ArrayList<GamePlayer>();

			for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
				GamePlayer gamePlayer = gameServer.getPlayerReadyList().get(i);
				gamePlayer.myCards.removeAllCard();
				gamePlayer.setPlayerStatus(GamePlayer.psPLAY);
				if (checkMoneyOfDealerPosition(gamePlayer.getPlayerId())) {
					dealerList.add(gamePlayer);
					PlayerInfo.lockMiniGame(gamePlayer.sInfo.uId, true);
				}
			}

			if (dealerList.size() == 0) {
				// Huy phong
				Debug.info("GameManager::doThiChuong room " + gameServer.getRoom().getId() + ":  HUY PHONG");
				gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_NO_DEALER);
				return false;
			}
			if (dealerList.size() == 1) {
				doNotifyNewDealer(dealerList.get(0).getPlayerId());
				Debug.info("GameManager::doThiChuong room " + gameServer.getRoom().getId()
						+ ":  Co duy nhat 1 dua du tien -> duoc lam chuong luon");
				return false;
			}
			setTurn(TurnType.CHOICE_DEALER);
			shuffleCard();
			dealTheCard(dealerList, ServerConstant.MAX_CARD_PER_PLAYER_IN_CHOICE_DEALER);

			boolean[] isPlaying = new boolean[ServerConstant.MAX_PLAYER];
			for (int i = 0; i < ServerConstant.MAX_PLAYER; i++) {
				GamePlayer player = gameServer.getPlayerByChair(i + 1);
				if (player.getPlayerStatus() == GamePlayer.psPLAY) {
					isPlaying[i] = true;
				} else {
					isPlaying[i] = false;
				}
			}

			GamePlayer newDealer = null;
			SendThiChuongResult msg = new SendThiChuongResult();
			msg.chair = new byte[dealerList.size()];
			msg.cardId = new byte[dealerList.size()];
			for (int i = 0; i < dealerList.size(); i++) {
				GamePlayer player = dealerList.get(i);
				if (newDealer == null) {
					newDealer = player;
				} else {
					if (player.myCards.getScore() > newDealer.myCards.getScore()) {
						newDealer = player;
					} else if (player.myCards.getScore() == newDealer.myCards.getScore()) {
						if (player.myCards.maxCard.compare(newDealer.myCards.maxCard)) {
							newDealer = player;
						}
					}

				}

				msg.chair[i] = (byte) (player.getPlayerId() - 1);
				msg.cardId[i] = (byte) player.myCards.maxCard.ID;
				msg.isPlaying = isPlaying;
			}
			setDealer(newDealer.getPlayerId());
			msg.dealerChair = (byte) (roomInfo.dealerPlayerId - 1);
			gameServer.sendMsg(msg);

			Debug.info("GameManager::doThiChuong room " + gameServer.getRoom().getId() + ":  END -> "
					+ msg.dealerChair);
			return true;
		}
	}

	private void setDealer(int playerId) {
		roomInfo.dealerPlayerId = (byte) playerId;
		int numPlayer = (gameServer.getPlayerReadyList().size() - 1);
		numPlayer = numPlayer == 0 ? 1 : numPlayer;
		int hs = roomInfo.hsCalculatorMaxBet * numPlayer;

		roomInfo.curMaxBet = (int) ((gameServer.getPlayerByChair(playerId).getGold() - roomInfo.chickenBet) / hs);
		roomInfo.curMaxBet = Math.min(roomInfo.curMaxBet, roomInfo.maxBet);

		lockMiniGame();
	}

	public void lockMiniGame() {
		for (GamePlayer gamePlayer : gameServer.playerList) {
			if (gamePlayer != null && gamePlayer.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
				if (gamePlayer.sInfo != null) {
					if (gamePlayer.isBot())
						continue;
					if (gamePlayer.getPlayerId() == roomInfo.dealerPlayerId) {
						PlayerInfo.lockMiniGame(gamePlayer.sInfo.uId, true);
						PlayerInfo.resetGoldKept(gamePlayer.sInfo.uId);
					} else {
						PlayerInfo.lockMiniGame(gamePlayer.sInfo.uId, false);
						PlayerInfo.resetGoldKeptToDefault(gamePlayer.sInfo.uId, -roomInfo.goldPlayer);
						PlayerInfo.resetGoldKeptRealToDefault(gamePlayer.sInfo.uId, roomInfo.goldPlayer);
					}
				}
			}
		}
	}

	private void doNotifyNewDealer(int playerId) {

		setDealer(playerId);
		SendNotifyChangeDealer msg = new SendNotifyChangeDealer();
		msg.chair = (byte) (playerId - 1);
		gameServer.sendMsg(msg);
	}

	public boolean checkMoneyOfDealerPosition(int playerId) {
		if (playerId < 1 || playerId > ServerConstant.MAX_PLAYER)
			return false;
		GamePlayer gamePlayer = gameServer.getPlayerByChair(playerId);
		if (gamePlayer == null)
			return false;
		if (gamePlayer.getPlayerStatus() == GamePlayer.psNO_LOGIN) {
			return false;
		}

		long goldNeedForPosition = roomInfo.goldDealer;

		boolean ret = gamePlayer.checkGold(goldNeedForPosition);
		return ret;
	}

	private void defineDealer() {
		// updateRoomOwner();
		// game.server.cmd.SendUpdateOwnerRoom cmdOwner = new
		// game.server.cmd.SendUpdateOwnerRoom();
		// cmdOwner.roomOwner = roomOwnerID - 1;
		// gameServer.sendMsg(cmdOwner);
		// trace("CHUYEN ROOM OWNER. " + roomOwnerID);
		//
		/*
		 * if (GameCount == 0) dealer = roomOwnerID; else { int player = dealer;
		 * GamePlayer gp = null; for (int i=1; i<=ServerConstant.MAX_PLAYER;
		 * i++) { player--; if (player <= 0) player = ServerConstant.MAX_PLAYER;
		 * gp = gameServer.getPlayerByChair(player); if (gp != null &&
		 * gp.isPlaying) break; } dealer = player; } return dealer;
		 */
	}

	public int getAutoStartCountdown() {
		return timeCountDown;
	}

	// public int getGameState() {
	// return gameState;
	// }

	// public void setGameState(int value) {
	// Debug.trace("GameManager::setGameState: " + value);
	// gameState = value;
	// }

	public int getRoomType() {
		return roomType;
	}

	public void setRoomType(int value) {
		roomType = value;
	}

	public int getRoomOwner() {
		return roomOwnerID;
	}

	public void setRoomOwner(int value) {
		roomOwnerID = value;
	}

	public long getMoneyBet() {
		return roomInfo.minBet;
	}

	public void setMoneyBet(long value) {
		moneyBet = value;
	}

	public int getBetType() {
		return betType;
	}

	public void setBetType(int value) {
		betType = value;
	}

	public int getBetIndex() {
		return betIndex;
	}

	public void setBetIndex(int value) {
		betIndex = value;
	}

	public long getMinGold() {
		return minGold;
	}

	public void setMinGold(long value) {
		minGold = roomInfo.goldPlayer;
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gs) {
		gameServer = gs;
	}

	public void ResetEndGame() {
		int i;
		for (i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			gameServer.getPlayerByChair(i).isReady = false;
			gameServer.getPlayerByChair(i).isSelectCard = false;
		}
	}

	public void reset() {
		// setGameState(gmNoPlay);
	}

	public void processPlayerResetKick(int nChair) {
		if (gameServer.getServerState() != GameServer.gsPlay)
			return;
		GamePlayer pPlayer = gameServer.getPlayerByChair(nChair);
		if (pPlayer != null && pPlayer.isPlaying) {
			pPlayer.isSelectCard = true;
		}
	}

	public void processNotifyConnect(int nChair) {
		GamePlayer pPlayer = gameServer.getPlayerByChair(nChair);
		if (pPlayer != null && pPlayer.isPlaying) {
			pPlayer.connect = true;
		}
	}

	public void processPlayerBinhReady(int nChair, ReceivePlayerReady pCmd) {
		// Send score user to all room

		// if (gameState != gmPlay)
		// return;
		// trace("processPlayerBinhReady. Player " + nChair);
		// int i = 0;
		// GamePlayer pPlayer = gameServer.getPlayerByChair(nChair);
		// if (pPlayer != null && !pPlayer.isReady) {
		// /*
		// * if (gameTime > 1) pPlayer.nKick = 2;
		// */
		// trace("NumCard " + pPlayer.myCards.GetNumOfGroupCards() + " "
		// + pCmd.NumCard + " " + pCmd.Cards.length);
		// for (int j = 0; j < pPlayer.myCards.GetNumOfGroupCards(); j++) {
		// boolean isDiff = true;
		// int ID = pPlayer.myCards.GroupCards().get(j).Cards().get(0).ID;
		// for (i = 0; i < pCmd.NumCard; i++) {
		// if (ID == pCmd.Cards[i]) {
		// isDiff = false;
		// break;
		// }
		// }
		// if (isDiff) {
		// SendNotifyPlayerUnReady cmd = new SendNotifyPlayerUnReady();
		// cmd.PlayerID = nChair - 1;
		// gameServer.sendMsg(cmd);
		// return;
		// }
		// }
		// pPlayer.myCards.Clear();
		// String str = "";
		// for (i = 0; i < pCmd.NumCard; i++) {
		// GroupCard c = new GroupCard();
		// Card tempCard = new Card();
		// tempCard.ID = pCmd.Cards[i];
		// str += pCmd.Cards[i] + " ";
		// c.AddCard(tempCard);
		// if (pPlayer.myCards.GetNumOfGroupCards() < 13)
		// pPlayer.myCards.AddGroupCard(c);
		// }
		// trace("CARDS " + str);
		// pPlayer.isReady = true;
		// pPlayer.myCards.ArrangeChi();
		// pPlayer.connect = true;
		// SendNotifyPlayerReady cmd = new SendNotifyPlayerReady();
		// cmd.PlayerID = nChair - 1;
		// gameServer.sendMsg(cmd);
		// int nReady = 0;
		// int nGamePlayer = 0;
		// for (i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
		// if (gameServer.getPlayerByChair(i).getPlayerStatus() ==
		// GamePlayer.psPLAY) {
		// ++nGamePlayer;
		// if (gameServer.getPlayerByChair(i).isReady)
		// ++nReady;
		// }
		// }
		// if (nReady == nGamePlayer) {
		// trace("PLAYER READY");
		// DoShowGameResult();
		// // stepEndGame = ServerConstant.STEP_GAME_RESULT;
		// }
		// }
	}

	public void updateRoomOwner() {
		GamePlayer player;
		if (roomOwnerID == -1) {
			for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
				player = gameServer.getPlayerByChair(i);
				if (player != null && player.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
					roomOwnerID = i;
					break;
				}
			}
		} else // chu room thoat -> chuyen luot cho nguoi ke tiep
		{
			if (roomType == ServerConstant.TABLE_NEWBIE) {
				int tmp = roomOwnerID;
				for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
					if (tmp >= ServerConstant.MAX_PLAYER + 1)
						tmp = 1;
					player = gameServer.getPlayerByChair(tmp);
					if (player != null && player.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
						roomOwnerID = tmp;
						break;
					}
					++tmp;
				}
			} else {
				for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
					roomOwnerID--;
					if (roomOwnerID <= 0)
						roomOwnerID = ServerConstant.MAX_PLAYER;
					player = gameServer.getPlayerByChair(roomOwnerID);
					if (player != null && player.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
						break;
					}
				}
			}
		}
	}

	// neu co bot roi, kick bot
	// neu dang cho tao bot, huy cho tao bot
	// neu tao phong, cho bot vao
	public void processPlayerJoin(int nChair) {
		GamePlayer pPlayer = gameServer.getPlayerByChair(nChair);
		// if (pPlayer == null || pPlayer.pInfo == null) return;
		if (pPlayer == null)
			return;
		if (roomType == ServerConstant.TABLE_NEWBIE)
			updateRoomOwner();
		else {
			trace("ROOM OWNER ID " + roomOwnerID);
			if (roomOwnerID == -1)
				updateRoomOwner();
			trace("++ROOM OWNER ID " + roomOwnerID);
		}

	}

	public void processViewInfo(User user) {
		trace("CmdDefine.CMD_REQUEST_VIEW_INFO");
		if (user != null) {
			Room r = user.getJoinedRoom();
			if (r != null) {
				SendNotifyViewGame cmd = new SendNotifyViewGame();
				// cmd.gameState = gameState;
				cmd.gameState = gameServer.getServerState();
				// if (gameState == gmPlay) {
				if (gameServer.getServerState() == GameServer.gsPlay) {
					for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
						GamePlayer pPlayer = gameServer.getPlayerByChair(i);
						if (pPlayer != null) {
							cmd.isPlaying[i - 1] = pPlayer.isPlaying; // nguoi
						}
					}

					// Tung viet them
					{
						List<DeckInfo> deckInfos = new ArrayList<DeckInfo>();
						for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
							GamePlayer player = gameServer.getPlayerReadyList().get(i);

							DeckInfo info = new DeckInfo();
							info.chair = (byte) (player.getPlayerId() - 1);
							info.isShowCard = player.isShowCard;
							info.cardIds = player.myCards.getMyCardID();

							info.isBet = player.isBet();
							info.goldBet = player.getBet();

							info.isChickedJoined = player.isChickenJoined;

							deckInfos.add(info);
						}

						cmd.deckInfos = deckInfos;
						cmd.goldInChicken = (int) (gameServer.playersChickenJoined.size() * getBetInChicken());
						cmd.turnTime = timeCountDown;
						cmd.turnType = (byte) curTurnType;
					}
					// Tung end
				}
				gameServer.sendMsg(cmd, user);
				trace("Send done");
			} else
				trace("Room null");
		} else
			trace("User null");
		trace("view info done");
	}

	public void processPlayerExit(int nChair) {
		if (nChair == roomOwnerID) {
			// if (gameState == gmNoPlay
			if (gameServer.getServerState() == GameServer.gsNoPlay || roomType == ServerConstant.TABLE_NEWBIE) {
				updateRoomOwner();
				// update room owner
				game.server.cmd.SendUpdateOwnerRoom cmdOwner = new game.server.cmd.SendUpdateOwnerRoom();
				cmdOwner.roomOwner = roomOwnerID - 1;
				// gameServer.baseSvr.sendUsers(cmdOwner,
				// room.getPlayersList());
				gameServer.sendMsg(cmdOwner);
				//
			}
		}

		GamePlayer pPlayer;
		pPlayer = gameServer.getPlayerByChair(nChair);

		// Xoa khoi danh sach nhung nguoi san sang choi trong van moi
		// if (gameState == gmNoPlay) {
		if (gameServer.getServerState() == GameServer.gsNoPlay) {
			gameServer.removePlayer(pPlayer);
		}

		// if (roomInfo.dealerPlayerId == nChair && gameState == gmNoPlay) {
		if (roomInfo.dealerPlayerId == nChair && gameServer.getServerState() == GameServer.gsNoPlay) {
			// Chuong thoat khoi phong
			roomInfo.dealerPlayerId = DEALER_UNKNOW;

			choiceDealer();
		}

		if (gameServer.getServerState() == GameServer.gsNoPlay) {
			lockMiniGame();
		}

		if (pPlayer == null)
			return;

		// if (gameState == gmNoPlay) {
		if (gameServer.getServerState() == GameServer.gsNoPlay) {
			checkAutoStartGame();
		} else {
			int count = CountCurrentNumPlaying();

			switch (count) {
			case 0:
				// if (gameState == gmPlay) {
				if (gameServer.getServerState() == GameServer.gsPlay) {
					trace("EXIT DoShowGameResult COUNT PLAYER == 0");
					DoShowGameResult();
					// stepEndGame = ServerConstant.STEP_GAME_RESULT;
				}
				// if (gameServer.playerCount <= 0) {
				// if (gameState == gmEndGame) {
				// trace("EXIT COUNT PLAYER == 0");
				// // GameResultExit();
				// if (roomType == ServerConstant.TABLE_NEWBIE) {
				// for (int i = 0; i < 100 && gameState == gmEndGame; i++)
				// GameResult();
				// } else {
				// for (int i = 0; i < 150 && gameState == gmEndGame; i++)
				// GameResult2();
				// }
				// }
				// }
				GameCount = 0;
				break;
			default:
				// if (gameState == gmPlay)
				if (gameServer.getServerState() == GameServer.gsPlay) {
					int nReady = 0;
					int nGamePlayer = 0;
					for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
						if (gameServer.getPlayerByChair(i).getPlayerStatus() == GamePlayer.psPLAY) {
							++nGamePlayer;
							if (gameServer.getPlayerByChair(i).isReady)
								++nReady;
						}
					}
					if (nReady == nGamePlayer) {
						trace("EXIT DoShowGameResult COUNT PLAYER == 1");
						DoShowGameResult();
						// stepEndGame = ServerConstant.STEP_GAME_RESULT;
					}
				}
				break;
			}
		}
	}

	private void resetAutoStartGame() {
		isAutoStartGame = false;
		// timeCountDown = 0;
	}

	public boolean hasDealer() {
		if (!needThiChuong && roomInfo.dealerPlayerId != DEALER_ENOUGHT_MONEY
				&& roomInfo.dealerPlayerId != DEALER_UNKNOW)
			return true;

		int dealerCounter = 0;
		for (GamePlayer gamePlayer : gameServer.getPlayerReadyList()) {
			if (gamePlayer.getPlayerId() < 1 || gamePlayer.getPlayerId() > ServerConstant.MAX_PLAYER) {
				continue;
			}
			if (checkMoneyOfDealerPosition(gamePlayer.getPlayerId())) {
				dealerCounter++;
			}
		}

		if (dealerCounter != 0)
			return true;
		return false;

	}

	public void checkAutoStartGame() {
		if (gameServer.getServerState() == GameServer.gsNoPlay) {
			int nCount = gameServer.getPlayerCount();
			if (nCount >= ServerConstant.MIN_PLAYER) {
				if (!isAutoStartGame) {
					// if (hasDealer()){
					// isAutoStartGame = true;
					// }
					// else {
					// isAutoStartGame = false;
					//// if (gameServer.getPlayerCount() ==
					// ServerConstant.MAX_PLAYER)
					// {
					//// gameServer.destroyTable();
					// }
					// }
					isAutoStartGame = true;
					// autoStartCountDown = ServerConstant.AUTO_START_TIME;
					setAutoStartCountdown(TurnTime.TIME_TO_NEW_GAME);

				}
			} else {
				isAutoStartGame = false;
				// autoStartCountDown = ServerConstant.AUTO_START_TIME;
				setAutoStartCountdown(TurnTime.TIME_TO_NEW_GAME);
				/*
				 * SendUpdateAutoStart cmd = new SendUpdateAutoStart();
				 * cmd.isAutoStart = isAutoStartGame; cmd.autoStartTime =
				 * autoStartCountDown; gameServer.sendMsg(cmd);
				 */

			}
			SendUpdateAutoStart cmd = new SendUpdateAutoStart();
			cmd.isAutoStart = isAutoStartGame;
			cmd.autoStartTime = timeCountDown;
			gameServer.sendMsg(cmd);

		} else {
			isAutoStartGame = false;
			setAutoStartCountdown(TurnTime.TIME_TO_NEW_GAME);

			game.server.cmd.SendUpdateAutoStart cmd = new game.server.cmd.SendUpdateAutoStart();
			cmd.isAutoStart = isAutoStartGame;
			cmd.autoStartTime = timeCountDown;
			gameServer.sendMsg(cmd);
		}
	}

	private void SendEndCards() {
		// GamePlayer player;
		// for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
		// player = gameServer.getPlayerByChair(i);
		// if (player != null && player.isPlaying) {
		// String str = "Player " + i + ": ";
		// SendEndCards cmd = new SendEndCards();
		// for (int j = 0; j < player.myCards.GetNumOfGroupCards(); j++) {
		// cmd.Cards[j] = (byte) player.myCards.GroupCards().get(j)
		// .Cards().get(0).ID;
		// str += cmd.Cards[j] + " ";
		// }
		// trace("SendEndCards " + str);
		// cmd.nCards = (byte) player.myCards.GetNumOfGroupCards();
		// cmd.PlayerID = (byte) (i - 1);
		// cmd.maubinh = (byte) player.maubinh;
		// gameServer.sendMsg(cmd);
		//
		// }
		// }
		// myPlayLog.writeEndCards(gameServer.getRoom());
	}

	public void DoShowGameResult() {
		// trace("DO SHOW GAME RESULT.");
		// if (gameState != gmPlay)
		// return;
		// gameState = gmEndGame;
		// gameTime = -1;
		// SendEndCards();
		//
		// if (roomType == ServerConstant.TABLE_NEWBIE) {
		// // myPlayLog.writeFormatResult1(gameServer.getRoom());
		// if (getNumMauBinh() > 0) {
		// stepEndGame = ServerConstant.SOBAI_MAUBINH;
		// ThuTuSoBai();
		// timeStep = 1;
		// trace("CO NGUOI MAU BINH");
		// } else if (getNumBinhLung() > 0) {
		// stepEndGame = ServerConstant.SOBAI_BINHLUNG;
		// ThuTuSoBai();
		// timeStep = 1;
		// trace("CO NGUOI BINH LUNG");
		// } else {
		// trace("SO CHI");
		// stepEndGame = ServerConstant.SOBAI_CHI;
		// ThuTuSoBai();
		// InitListBinh();
		// trace("DoShowGameResult. ComPareSoChi");
		// CompareSoChi();
		// timeStep = 1;
		// Chi = 1;
		// index = 0;
		// // Send Binh Chi Index
		// SendBinhChiIndex cmd2 = new SendBinhChiIndex();
		// cmd2.chi = Chi;
		// gameServer.sendMsg(cmd2);
		// trace("timeStep " + timeStep + " Chi " + Chi + " index "
		// + index);
		// }
		// /*
		// * if (getNumNormal() <= 1) { //them 2 dong trang result2,3
		// * //myPlayLog.writeFormatResultEmpty(gameServer.getRoom(), 2);
		// * //myPlayLog.writeFormatResultEmpty(gameServer.getRoom(), 3); }
		// */
		// } else {
		// // myPlayLog.writeFormatResultModeAt(gameServer.getRoom());
		// if (getNumMauBinh() > 0) {
		// stepEndGame = ServerConstant.BINH_BAI;
		// ThuTuSoBai2();
		// timeStep = 1;
		// trace("BINH AT: SO BAI " + getNumMauBinh());
		// } else {
		// trace("BINH AT: SO CHI");
		// stepEndGame = ServerConstant.BINH_CHI;
		// ThuTuSoBai2();
		// timeStep = 1;
		// Chi = 0;
		// // Send Binh Chi Index
		// SendBinhChiIndex cmd2 = new SendBinhChiIndex();
		// cmd2.chi = Chi;
		// gameServer.sendMsg(cmd2);
		// trace("timeStep " + timeStep + " Chi " + Chi + " index "
		// + index);
		// }
		// /*
		// * if (getNumNormal() + getNumBinhLung() <= 1) { //them 2 dong trang
		// * result2,3 myPlayLog.writeFormatResultEmpty(gameServer.getRoom(),
		// * 2); myPlayLog.writeFormatResultEmpty(gameServer.getRoom(), 3); }
		// */
		// }
	}

	public int getNumPlayer() {
		int count = 0;
		GamePlayer pPlayer;
		for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
			pPlayer = gameServer.getPlayerByChair(i);
			if (pPlayer == null || !pPlayer.isPlaying)
				continue;
			count++;
		}
		return count;
	}

	private boolean CompareMoney(long money1, long money2) {
		// money1 += cheat; //dung de test
		if (Math.abs(money1) != Math.abs(money2))
			return false;
		return true;
	}

	/*
	 * public void destroyTable() { for (int i=1; i<=ServerConstant.MAX_PLAYER;
	 * i++) { GamePlayer gp = gameServer.getPlayerByChair(i); if (gp != null &&
	 * gp.isPlaying) { PlayerInfo.setIsHold((int)gp.sInfo.uId, false); if
	 * (gp.getUser() != null) { gameServer.kickUserFromRoom(gp.getUser(),
	 * SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR); PlayerInfo pInfo =
	 * gp.getPlayerInfo(); if (pInfo != null) MetricLog.writeDestroyTable((int)
	 * gp.sInfo.uId, gp.sInfo.device, pInfo.bean, pInfo.coin,
	 * gameServer.getRoomId()); } gameState = gmNoPlay; } }
	 * gameServer.serverState = gameServer.gsNoPlay; Room room =
	 * gameServer.getRoom(); if (room != null) { Zone zone = room.getZone();
	 * zone.removeRoom(room); } }
	 */

	public void processConfigCard(ReceiveConfigCards pCmd) {
		String str = "";
		for (int i = 0; i < pCmd.cards.length; i++) {
			ConfigCards[i] = pCmd.cards[i];
			str += pCmd.cards[i] + " ";
		}
		trace("Cheat Cards " + str);
		isConfigCard = true;
		if (ConfigCards.length <= 0)
			isConfigCard = false;
	}

	private void customKick() {
		int i;
		// User user = null;
		PlayerInfo pInfo = null;
		for (i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer pPlayer = gameServer.getPlayerByChair(i);
			if (pPlayer == null || !pPlayer.isPlaying || pPlayer.getPlayerStatus() == GamePlayer.psLOOKON
					|| pPlayer.getPlayerStatus() == GamePlayer.psNO_LOGIN) {
				continue;
			}
			// user = getUser(PlayerUid[i]);
			// user = _user[i];
			// pInfo = pPlayer.getPlayingInfo();
			pInfo = pPlayer.getPlayerInfo();
			if (pInfo != null && pPlayer.getUser() != null) {
				trace(pPlayer.sInfo.name + " Connected " + pPlayer.getUser().isConnected());
				// if (pPlayer.nKick == 0 || !user.isConnected())
				/*
				 * if (pPlayer.nKick == 0 || (!pPlayer.isConnect &&
				 * !pPlayer.getUser().isMobile())) //if (!pPlayer.isConnect) {
				 * gameServer.kickUserFromRoom(pPlayer.getUser(),
				 * SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i); continue; }
				 */

				// if (pPlayer.isPlaying && !pPlayer.isReady) {
				// gameServer.kickUserFromRoom(pPlayer.getUser(),
				// SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
				// continue;
				// }
				trace(i + " Player " + pInfo.uName + " bean " + pInfo.bean + " money bet " + moneyBet);
				Zone zone = gameServer.getRoom().getZone();
				if (zone != null) {
					if (roomType == ServerConstant.TABLE_ADVANCE) {
						GameChannel gc = (GameChannel) zone.getProperty(ServerConstant.CHANNEL_LOGIC);
						if (gc != null) {
							if (gc.groupIndex > 1) {
								if (pInfo.bean < ServerConstant.TIMES_MIN_BET2 * (long) moneyBet) {
									gameServer.kickUserFromRoom(pPlayer.getUser(),
											SendKickFromRoom.Reason.eKICK_REASON_MONEY, i);
									continue;
								}
							}
						}
					}
					if (pInfo.bean < ServerConstant.TIMES_MIN_BET * (long) moneyBet) {
						// kick user ra khoi room
						if (pInfo.isBot()) {
							gameServer.kickUserFromRoom(pPlayer.user, SendKickFromRoom.Reason.eKICK_REASON_MONEY, i);
						} else
							gameServer.kickUserFromRoom(pPlayer.getUser(), SendKickFromRoom.Reason.eKICK_REASON_MONEY,
									i);
						continue;
					}
				}

				if (pPlayer.connect == false) {
					gameServer.kickUserFromRoom(pPlayer.getUser(), SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
					Debug.trace("kick khong tuong tacc qua lau  hoac disconnect): " + pInfo.getUId());
					continue;
				}
			}
		}

		gameServer.doUserReqQuitRoom();
		
		trace("PLAYER COUNT " + gameServer.getPlayerCount());
	}

	/*
	 * public void processPlayerReady(int nChair) { if (nChair == roomOwnerID)
	 * checkGameStart(); }
	 */

	private boolean canGameStart() {
		int count = gameServer.getPlayerCount();
		if (count >= ServerConstant.MIN_PLAYER) {
			// if (hasDealer())
			return true;
		}

		// if (count == ServerConstant.MAX_PLAYER){
		// gameServer.destroyTable();
		// }

		return false;
	}

	public void checkGameStart() {
		if (roomInfo.dealerPlayerId > ServerConstant.MAX_PLAYER) {
			return;
		}
		kickUserEnoughMoneyStart();
		if (gameServer.getServerState() == GameServer.gsNoPlay && canGameStart()) {
			resetAutoStartGame();
			startGame();
		}
	}

	private void startGame() {
		if (gameServer.startGame()) {

			gameServer.setServerState(GameServer.gsPlay);
			if (needThiChuong) {
				if (doThiChuong()) {
					return;
				}
			}
			lockMiniGame();

			doStartGame();
		}
	}

	private void doStartGame() {
		timeStart = new Date().getTime();
		this.codeVanChoi  = this.gameServer.getRoomId() + "_" + System.currentTimeMillis();
		trace("GAME START");
		resetGameStart();

		shuffleCard();

		dealTheCard(gameServer.getPlayerReadyList(), ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME);
		// DistributedCard();
		// OnLoading();

		boolean[] isPlaying = new boolean[ServerConstant.MAX_PLAYER];
		for (int i = 0; i < ServerConstant.MAX_PLAYER; i++) {
			GamePlayer player = gameServer.getPlayerByChair(i + 1);
			// Debug.trace("GameManager::startGame: " + i + " " +
			// player.getPlayerStatus());
			if (player.getPlayerStatus() == GamePlayer.psPLAY) {
				isPlaying[i] = true;
			} else {
				isPlaying[i] = false;
			}
		}

		SendNotifyGameStart msg = new SendNotifyGameStart();
		msg.dealer = (byte) (roomInfo.dealerPlayerId - 1);
		msg.gameTime = gameTime;
		msg.curMaxBet = roomInfo.curMaxBet;
		msg.isPlaying = isPlaying;
		msg.nGameCount = 0;
		gameServer.sendMsg(msg);

		setTurn(TurnType.GAME_START);
	}

	@SuppressWarnings("unused")
	private void UserTimeDown() {
		for (int i = 1; i < ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = gameServer.getPlayerByChair(i);
			if (gp != null && gp.getPlayerStatus() != GamePlayer.psNO_LOGIN
					&& gp.getPlayerStatus() != GamePlayer.psLOOKON) {
				if (gp.timeDown > 0 && !gp.reqQuitRoom) {
					gp.timeDown--;
					if (gp.timeDown == 0) {
						gp.reqQuitRoom = true;
						// if (gameState == gmPlay || gameState == gmEndGame) {
						if (gameServer.getServerState() == GameServer.gsPlay) {
							SendNotifyReqQuitRoom cmd = new SendNotifyReqQuitRoom();
							cmd.chair = (i - 1);
							cmd.reason = SendNotifyReqQuitRoom.REQ_SUCCESS;
							gameServer.sendMsg(cmd);
						} else
							gameServer.kickUserFromRoom(gp.getUser(),
									game.server.cmd.SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
					}
				}
			}
		}
	}

	public void gameLoop() {
		// if (gameState == gmPlay) {
		// Debug.trace("GameManager::gameLoop: " + timeCountDown);
		if (gameServer.getServerState() == GameServer.gsPlay) {
			if (timeCountDown > 0) {
				timeCountDown--;
			}
			if (timeCountDown == 0) {
				onAutoChangeTurn();
			}
			if (ServerConstant.ENABLE_BOT) {
				loopBot();
			}
			// } else if (gameState == gmNoPlay) {
		} else if (gameServer.getServerState() == GameServer.gsNoPlay) {
			// if (curTurnType == TurnType.CHOICE_DEALER) {
			// if (timeCountDown > 0){
			// timeCountDown--;
			// }
			// if (timeCountDown == 0)
			// {
			// onAutoChangeTurn();
			// }
			// }
			// else
			if (isAutoStartGame) {
				if (timeCountDown > 0) {
					timeCountDown--;
					if (timeCountDown == TurnTime.TIME_TO_NEW_GAME - 1) {
						SendUpdateAutoStart cmd = new SendUpdateAutoStart();
						cmd.isAutoStart = true;
						cmd.autoStartTime = timeCountDown;
						gameServer.sendMsg(cmd);
					}
					if (timeCountDown == 0) {
						checkGameStart();
						// curTurnType = TurnType.GAME_START;
						// timeCountDown = TurnTime.TIME_TO_BET_START;
					}
				}
			}
		}

	}

	private void onAutoChangeTurn() {

		Debug.trace("GameManager::onAutoChangeTurn: " + curTurnType);
		//

		switch (curTurnType) {
		case TurnType.GAME_START:
			setTurn(TurnType.BET_START);
			doBetStart();
			break;
		case TurnType.BET_START:
			setTurn(TurnType.BET_END);
			doSendCard();// Step nan bai
			break;
		case TurnType.BET_END:
			setTurn(TurnType.GAME_RESULT);
			doShowCard();
			doShowGameResult();
			break;
		case TurnType.GAME_RESULT:
			setTurn(TurnType.GAME_END);
			doGameEnd();
			break;
		case TurnType.GAME_END:
			break;
		case TurnType.CHOICE_DEALER:
			doStartGame();
			break;
		}

	}

	// private void doGameStart() {
	// startGame();
	// }

	private void doBetStart() {
		SendNotifyBetStart msg = new SendNotifyBetStart();
		msg.turnTime = timeCountDown;
		gameServer.sendMsg(msg);

		for (GamePlayer gamePlayer : gameServer.getPlayerReadyList()) {
			if (gamePlayer.getPlayerId() == roomInfo.dealerPlayerId) {
				continue;
			}
			Debug.trace("doBetStart 1267:" + gamePlayer.getPlayerId());
			gamePlayer.betStart(timeCountDown);
		}
	}

	private void doSendCard() {
		// Kiem tra xem dua nao chua dat cuoc -> Tu dong dat cuoc ho no
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);
			if (!player.isBet() && player.getPlayerId() != roomInfo.dealerPlayerId) {
				autoBetForPlayer(player);
			}
		}

		// Gui bai ve cho tung nguoi choi
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);

			SendPlayerCard msg = new SendPlayerCard();
			msg.cardIds = player.myCards.getMyCardID();
			msg.time = timeCountDown;
			gameServer.sendMsg(msg, player.getUser());

			Debug.trace("doSendCard 1290:" + player.getUser() + "|" + msg);

		}

		for (GamePlayer gamePlayer : gameServer.getPlayerReadyList()) {
			if (gamePlayer.isBot()) {
				gamePlayer.showCardStart(timeCountDown);
			}
		}
	}

	public void doShowGameResult() {
//		Debug.trace("GameManager::doShowGameResult: " + gameServer.getRoom().getId());
//		Debug.info("GameManager::doShowGameResult room " + gameServer.getRoom().getId() + ":  Game result");
		List<GameResult> gameResults = new ArrayList<GameResult>();

		if (roomInfo.dealerPlayerId == -1) {
			Debug.trace("");
		}

		GamePlayer chuong = gameServer.getPlayerByChair(roomInfo.dealerPlayerId);

		// So bai voi chuong
		long goldDealer = 0;// tien chuong
		boolean anTat = true;
		boolean phatLuong = true;
		List<GamePlayer> dealerList = new ArrayList<GamePlayer>();
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);
			player.metricGoldKept = PlayerInfo.getGoldKept(player.sInfo.uId);
			GameResult result = new GameResult();
			if (player == chuong)
				continue;
			if (player.myCards.checkForDealer()) {
				dealerList.add(player);
			}

			long bet = player.getBet();
			int factor;
			if (chuong.myCards.compare(player.myCards)) {
				// Chuong an
				factor = getFactor(chuong.myCards);

				result.isWin = false;
				phatLuong = false;

				long dealerReceive = Math.min(bet * factor, player.getGold() + player.getGoldTemp() + bet);
				goldDealer += dealerReceive;

				long goldExtra = (factor - 1) * bet;
				if (goldExtra != 0) {
					// Tra tien sap khi ko du tien
					goldExtra = Math.min(goldExtra, player.getGold() + player.getGoldTemp());
					player.addGold(gameServer.getRoomId(), -goldExtra, GamePlayer.REASON_END_BET,0);
				}
				player.updateGoldWin(-bet - goldExtra, 0);
			} else {
				// Nguoi choi an
				factor = getFactor(player.myCards);
				long dealerLose = bet * factor;
				dealerLose = Math.min(dealerLose, chuong.getGold() + chuong.getGoldTemp() + goldDealer);
				goldDealer -= dealerLose;
				result.isWin = true;

				player.addGold(gameServer.getRoomId(), bet, GamePlayer.REASON_BACK_BET,0);
				int fee = (int) (dealerLose * roomInfo.comission * 0.01);
				double goldWin = dealerLose - fee;
				goldWin = Math.ceil(goldWin);
				player.addGold(gameServer.getRoomId(), (long) goldWin, GamePlayer.REASON_END_BET,fee);
				anTat = false;

				player.updateGoldWin((long) goldWin, fee);
			}
			result.gold = bet;
			result.chair = player.getPlayerId() - 1;
			result.score = player.myCards.getScore();
			result.factor = (byte) factor;
			gameResults.add(result);
		}
		int fee = goldDealer > 0 ?(int) (goldDealer * roomInfo.comission * 0.01) : 0;
		long goldWinChuong = goldDealer - fee;
		
		chuong.addGold(gameServer.getRoomId(), goldWinChuong, GamePlayer.REASON_END_BET,fee);
		chuong.updateGoldWin(goldWinChuong,fee);

		// Tim dua an trong ga
		GamePlayer winChicken = null;
		for (int i = 0; i < gameServer.playersChickenJoined.size(); i++) {

			GamePlayer player = gameServer.playersChickenJoined.get(i);
			if (winChicken == null) {
				winChicken = player;
				continue;
			}
			if (!winChicken.myCards.compare(player.myCards)) {
				winChicken = player;
			}
		}
		//tien an ga sau phe ( tru minh ra)
		if (winChicken != null) {
			int goldWin = (int) ((gameServer.playersChickenJoined.size() - 1) * getBetInChicken());
			long total = goldWin + getBetInChicken();
			int feeChicken = (int) (goldWin * roomInfo.comission * 0.01);
			winChicken.addGold(gameServer.getRoomId(), total-feeChicken, GamePlayer.REASON_END_BET_CHICKEN,feeChicken);
			winChicken.updateGoldWin(total, feeChicken);
		}

		SendNotifyGameResult msg = new SendNotifyGameResult();

		if (winChicken != null) {
			msg.chairChicken = (byte) (winChicken.getPlayerId() - 1);
			msg.goldChicken = gameServer.playersChickenJoined.size() * getBetInChicken();
		} else {
			msg.chairChicken = -1;
			msg.goldChicken = -1;
		}

		msg.dealer = (byte) (roomInfo.dealerPlayerId - 1);
		msg.goldDealer = goldDealer;
		msg.scoreDealer = chuong.myCards.getScore();
		msg.factorDealer = (byte) getFactor(chuong.myCards);
		if (phatLuong) {
			msg.statusDealer = SendNotifyGameResult.DEALER_PHAT_LUONG;
		} else if (anTat) {
			msg.statusDealer = SendNotifyGameResult.DEALER_CA_LANG_SANG_TIEN;
		} else {
			if (goldDealer > 0) {
				msg.statusDealer = SendNotifyGameResult.DEALER_WIN;
			} else if (goldDealer < 0) {
				msg.statusDealer = SendNotifyGameResult.DEALER_LOSE;
			} else if (goldDealer == 0) {
				msg.statusDealer = SendNotifyGameResult.DEALER_HOA;
			}
		}

		msg.gameResults = gameResults;
		gameServer.sendMsg(msg);

		processEndGameBetExtra();

		for (GamePlayer gamePlayer : gameServer.getPlayerReadyList()) {
			if (gamePlayer != null) {
				if (!gamePlayer.canCalculate()) {
					gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_MONEY);
					MetricLog.calMoneyError(gamePlayer.getUserId(), gameServer.getRoomId(), gamePlayer.getGoldTemp(), PlayerInfo.getGold(gamePlayer.getUserId()), codeVanChoi);
					return;
				}
			}
		}
		
		try {
			Thread.sleep(TimeUnit.SECONDS.toMillis(5));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		for (GamePlayer gamePlayer : gameServer.getPlayerReadyList()) {
			if (gamePlayer != null) {
				gamePlayer.updateGoldChange();
			}
		}

		// Xu ly an hu
		for (GamePlayer gamePlayer : gameServer.playerList) {
			if (gamePlayer != null && gamePlayer.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
				if (gamePlayer.sInfo != null) {
					if (gamePlayer.getPlayerStatus() != GamePlayer.psPLAY)
						continue;
					if (gamePlayer.myCards.isAnHu()) {
						for (Card card : gamePlayer.myCards.myCard) {
							Debug.trace("GameManager::doShowGameResult: " + card.getName());
						}
						gameServer.SendTrungHu(gamePlayer.getUserId(), gamePlayer.sInfo.name, gameServer.getRoom(),
								(int) gameServer.getMoneyBet(), HuConstant.Hu_Reason.TRUNG_HU,
								gamePlayer.myCards.getMyCardIDInt(), gamePlayer.myCards.getCuocHu());
					}
				}
			}
		}
	}

	private void autoBetForPlayer(GamePlayer player) {
		long bet = roomInfo.minBet;
		player.updateAfk();
		// if (bet != 0) {
		// if (!player.checkGold(bet)) {
		// //So tien dat cuoc o van truoc nhieu hon so tien hien tai
		// bet = roomInfo.minBet;
		// }
		// } else if (bet == 0) {
		// bet = roomInfo.minBet;
		// }
		gameServer.doBet((byte) (player.getPlayerId()), bet);
	}

	private void doShowCard() {
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);

			if (!player.isShowCard) {
				player.isShowCard = true;
				SendPlayerShowCard msg = new SendPlayerShowCard();
				msg.chair = (byte) (player.getPlayerId() - 1);
				msg.cardId = player.myCards.getMyCardID();
				gameServer.sendMsg(msg);
			}
		}
	}

	public void doGameEnd() {

		Debug.info("GameManager::doGameEnd room " + gameServer.getRoom().getId() + ":  ");

		List<GamePlayer> dealerList = new ArrayList<GamePlayer>();
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);

			if (player.myCards.checkForDealer()) {
				dealerList.add(player);
			}
		}

		// Cap nhat lai so tien sau van choi
		synchronized (gameServer.getPlayerReadyList()) {
			for (GamePlayer player : gameServer.getPlayerReadyList()) {

				if (player.getPlayerId() < 1 || player.getPlayerId() > ServerConstant.MAX_PLAYER)
					continue;
				SendUpdatePlayerData msg1 = new SendUpdatePlayerData();
				msg1.chair = (byte) (player.getPlayerId() - 1);
				msg1.gold = player.getGold();
				gameServer.sendMsg(msg1);
			}
		}

		SendNotifyGameEnd msg = new SendNotifyGameEnd();
		gameServer.sendMsg(msg);

		// Xu ly van moi

		// setGameState(gmNoPlay);
		gameServer.setServerState(GameServer.gsNoPlay);
		resetAutoStartGame();
		gameServer.endGame();

		kickBot();

		doUserReqQuitRoom();
		// customKick();
		kickPlayerAfk();
		kickPlayerDisconnect();
		kickUserEnoughMoney();

		gameServer.processWatcherTobeUser();
		// addBeanFromSmS();

		gameServer.refreshPlayerReadyList();
		// Chuyen chuong khi co thang 10
		if (dealerList.size() != 0) {
			processChoiceNewDealer(dealerList);
		} else {
			if (checkMoneyOfDealerPosition(roomInfo.dealerPlayerId)) {
				//
				setDealer(roomInfo.dealerPlayerId);
			} else {
				roomInfo.dealerPlayerId = DEALER_ENOUGHT_MONEY;
				choiceDealer();
			}
		}

		checkAutoStartGame();

	}

	private void processChoiceNewDealer(List<GamePlayer> dealerList) {
		GamePlayer newDealer = null;
		for (int i = 0; i < dealerList.size(); i++) {
			GamePlayer player = dealerList.get(i);
			if (!checkMoneyOfDealerPosition(player.getPlayerId()))
				continue;
			if (newDealer == null) {
				newDealer = player;
				continue;
			}
			if (player.myCards.compare(newDealer.myCards)) {
				newDealer = player;
			}
		}
		if (newDealer != null) {
			doNotifyNewDealer(newDealer.getPlayerId());
		}
	}
	/**
	 * ham xu ly ket qua danh bien
	 */
	private void processEndGameBetExtra() {
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);

			List<GameResult> gameResults = new ArrayList<GameResult>();

			for (Entry<GamePlayer, Integer> entry : player.betExtraList.entrySet()) {
				GamePlayer player1 = entry.getKey();
				Integer betIdx = entry.getValue();

				GameResult result = new GameResult();
				result.chair = player1.getPlayerId() - 1;
				boolean ownerWin = player.myCards.compare(player1.myCards);
				result.isWin = !ownerWin;// Minh thang -> thang result.chair
											// thua
				result.gold = getGoldBetExtra(betIdx);

				int factor = 1;

				result.factor = (byte) factor;

				int roomId = gameServer.getRoom().getId();

				gameResults.add(result);

				if (ownerWin) {
					int goldWin = (int) (result.gold * factor);
					int fee = (int) (goldWin * roomInfo.comission * 0.01);
					long goldReceive = goldWin-fee;
					player.addGold(roomId, goldReceive, GamePlayer.REASON_END_BET_EXTRA,
							String.valueOf(player1.getUserId()),fee);
					player.addGold(roomId, result.gold, GamePlayer.REASON_BACK_BET_EXTRA,
							String.valueOf(player1.getUserId()),0);
					
					player.updateGoldWin(goldReceive, fee);
					
//					player1.updateGoldWin(-goldWin, 0);
					
					//
//					player1.addGold(roomId, 0, GamePlayer.REASON_END_BET_EXTRA,
//							String.valueOf(player.getUserId()),0);
					
				}
				
				if (factor != 1) {
					if (ownerWin) {
						player1.addGold(roomId, -result.gold * (factor - 1), GamePlayer.REASON_X_SCORE,
								String.valueOf(player.getPlayerInfo().getUId()),0);
					} else {
						// player.addGold(roomId, -result.gold * (factor - 1),
						// GamePlayer.REASON_X_SCORE,
						// String.valueOf(player1.getPlayerInfo().getUId()));
					}
				}
			}

			SendBetExtraResult msg = new SendBetExtraResult();
			msg.gameResults = gameResults;
			gameServer.sendMsg(msg, player.getUser());
		}
	}

	public void kickPlayerDisconnect() {
		for (int i = 1; i < gameServer.playerList.size(); i++) {
			GamePlayer gamePlayer = gameServer.getPlayerByChair(i);
			if (gamePlayer == null) {
				continue;
			}
			if (gamePlayer.getPlayerStatus() == GamePlayer.psNO_LOGIN) {
				continue;
			}
			PlayerInfo playerInfo = gamePlayer.getPlayerInfo();
			if (playerInfo != null && gamePlayer.getUser() == null && !gamePlayer.isBot()) {
				if (gamePlayer.user == null) {
					gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
					Debug.info("GameManager::kickPlayerDisconnect room " + gameServer.getRoom().getId()
							+ ":  LOI ko kick duoc user disconnect");
					return;
				}
				gameServer.kickUserFromRoom(gamePlayer.user, SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
				Debug.info("GameManager::kickPlayerDisconnect room " + gameServer.getRoom().getId() + ":  "
						+ gamePlayer.getPlayerInfo().getUId());
				gameServer.onGameUserExit(gamePlayer.user, i);
				if (gameServer.removePlayer(gamePlayer)) {
					i--;
				}
			}

		}
		// for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
		// GamePlayer player = gameServer.getPlayerReadyList().get(i);
		// if (player.isBot())
		// continue;
		// if (player.getUser() == null) {
		// Debug.info("GameManager::kickPlayerDisconnect room " +
		// gameServer.getRoom().getId() + ": " +
		// player.getPlayerInfo().getUId());
		// gameServer.kickUserFromRoom(player.user,
		// SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, player.getPlayerId());
		//
		// gameServer.onGameUserExit(player.user, player.getPlayerId());
		//
		// if (gameServer.removePlayer(player)) {
		// i--;
		// }
		// }
		// }
	}

	private void kickUserEnoughMoney() {
		for (int i = 0; i < gameServer.playerList.size(); i++) {
			GamePlayer gamePlayer = gameServer.getPlayerByChair(i);
			if (gamePlayer == null || gamePlayer.getPlayerInfo() == null) {
				continue;
			}

			PlayerInfo playerInfo = gamePlayer.getPlayerInfo();
			Debug.trace("GameManager checkGold = " + gamePlayer.checkGold(roomInfo.goldPlayer));
			trace("kickUserEnoughMoney",playerInfo.getUId(),gamePlayer.getGold(),gamePlayer.getGoldTemp(),roomInfo.goldPlayer);
			if (playerInfo != null && !gamePlayer.checkGold(roomInfo.goldPlayer)) {
				User u = gamePlayer.getUser() != null ? gamePlayer.getUser() : gamePlayer.user;
				if (u != null) {
					gameServer.kickUserFromRoom(u, SendKickFromRoom.Reason.eKICK_REASON_MONEY, i);
				} else {
					gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
					return;
				}
			}
		}
	}
	private void kickUserEnoughMoneyStart() {
		for (int i = 0; i < gameServer.playerList.size(); i++) {
			GamePlayer gamePlayer = gameServer.getPlayerByChair(i);
			if (gamePlayer == null || gamePlayer.getPlayerStatus()==GamePlayer.psNO_LOGIN) {
				continue;
			}

			PlayerInfo playerInfo = null;
			try {
				playerInfo = PlayerInfo.getCurrentPlayerInfo(gamePlayer.getUserId());
			} catch (DataControllerException e) {
				e.printStackTrace();
			}

			if (playerInfo != null && !BotManager.isBot(playerInfo.getUId()) && !gamePlayer.checkGold(roomInfo.goldPlayer,(int) playerInfo.bean)) {
				User tmp = gamePlayer.getUser() != null ? gamePlayer.getUser() : gamePlayer.user;
				if (tmp != null) {
					gameServer.kickUserFromRoom(tmp, SendKickFromRoom.Reason.eKICK_REASON_MONEY, i);
				}else {
					gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_MONEY);
					return;
				}
			}
		}
	}

	private void kickPlayerAfk() {

		for (int i = 0; i < gameServer.playerList.size(); i++) {
			GamePlayer gamePlayer = gameServer.getPlayerByChair(i);
			if (gamePlayer == null) {
				continue;
			}
			if (gamePlayer.getPlayerStatus() == GamePlayer.psNO_LOGIN) {
				continue;
			}

			PlayerInfo playerInfo = gamePlayer.getPlayerInfo();
			if (playerInfo != null && gamePlayer.isAfk()) {
				if (gamePlayer.getUser() != null) {
					gameServer.kickUserFromRoom(gamePlayer.getUser(), SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
				} else if (gamePlayer.user != null) {
					gameServer.kickUserFromRoom(gamePlayer.user, SendKickFromRoom.Reason.eKICK_REASON_NO_PLAY, i);
				} else {
					gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
					Debug.info("GameManager::kickPlayerAfk room " + gameServer.getRoom().getId()
							+ ":  LOI ko kick duoc user AFK");
					return;
				}
			}
		}
	}

	private void loopBot() {
		GamePlayer pl = null;
		for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
			pl = gameServer.getPlayerByChair(i);
			if (pl != null && pl.isLogin()) {
				if (!pl.isReady && pl.getPlayerInfo() != null && pl.getPlayerInfo().isBot()) {
					pl.onLoop();
				}
			}
		}
	}

	private void resetGameStart() {
		// setGameState(gmPlay);
		gameServer.setServerState(GameServer.gsPlay);
		if (roomType == ServerConstant.TABLE_NEWBIE)
			gameTime = ServerConstant.GAME_TIME;
		else
			gameTime = ServerConstant.GAME_TIME2;

		// reset logic
		// ResetEndGame();
	}

	private BaCayExtension getExtension() {
		return (BaCayExtension) ExtensionUtility.getExtension();
	}

	/**
	 * edit user score
	 *
	 * @param chair
	 * @param
	 * @param score
	 * @param resultType
	 *            0: ko cong gi, 1:win, 2:lost
	 * @param isQuiter
	 * @return so bean bi thie'u
	 */
	private long updateMoney(int chair, long money, long score, int resultType, boolean isQuiter, boolean saveDb) {
		return 0;
	}

	private int CountCurrentNumPlaying() {
		int count = 0;
		int i;
		for (i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer player = gameServer.getPlayerByChair(i);
			if (player != null) {
				if (player.getPlayerStatus() == GamePlayer.psPLAY)
					count++;
			}
		}
		return count;
	}

	public void DoActionEndGame() {
		if (gameServer.getServerState() == GameServer.gsNoPlay)
			// if (gameState == gmNoPlay)
			return;
		gameServer.setServerState(GameServer.gsNoPlay);
		// setGameState(gmNoPlay);
		gameServer.endGame();
		if (roomType == ServerConstant.TABLE_ADVANCE)
			defineDealer();
		doUserReqQuitRoom();
		customKick();

		/*
		 * trace ("USER->VIEWER"); gameServer.processUserTobeViewer();
		 */
		trace("VIEWER->USER");
		gameServer.processWatcherTobeUser();

		checkAutoStartGame();
//		MetricLog.writePlayTime(moneyBet, (new Date().getTime() - timeStart) / 1000);
	}

	public int getNumPlaying() {
		int nPlayer = 0;
		GamePlayer gp = null;
		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			gp = gameServer.getPlayerByChair(i);
			if (gp != null && gp.getPlayerStatus() == GamePlayer.psPLAY)
				nPlayer++;
		}
		return nPlayer;
	}

	private void doUserReqQuitRoom() {
		trace("doUserReqQuitRoom");
		GamePlayer gp = null;
		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			gp = gameServer.getPlayerByChair(i);
			if (gp != null) {
				trace("reqQuitRoom " + gp.reqQuitRoom);
				if (gp.reqQuitRoom) {
					User u = gp.getUser();
					if (gp.isBot()) {
						u = gp.user;
					}
					if (u != null) {
						/*
						 * if (gp.timeDown == 0) { SendUserIdle cmd = new
						 * SendUserIdle(); gameServer.sendMsg(cmd, u); }
						 */
						ExtensionUtility.instance().outRoom(u);
					}
				}
			}
		}
	}

	public void processUserReqQuitRoom(User user) {
		trace(user.getName() + " processUserReqQuitRoom");
		SendNotifyReqQuitRoom cmd = new SendNotifyReqQuitRoom();
		cmd.chair = user.getPlayerId() - 1;
		cmd.reason = SendNotifyReqQuitRoom.REQ_SUCCESS;
		if (gameServer.getServerState() == GameServer.gsNoPlay || user.getJoinedRoom() == null) {
			// if (gameState == gmNoPlay || user.getJoinedRoom() == null) {
			cmd.reason = SendNotifyReqQuitRoom.REQ_FAIL;
		}
		if (user.getPlayerId() >= 1 && user.getPlayerId() <= ServerConstant.MAX_PLAYER) {
			GamePlayer gp = gameServer.getPlayerByChair(user.getPlayerId());
			trace("VAO DAY ROAI " + gp.getPlayerStatus());
			if (gp != null && gp.getPlayerStatus() == GamePlayer.psPLAY) {
				gp.reqQuitRoom = true;
				trace(user.getName() + " processUserReqQuitRoom. reqQuitRoom " + gp.reqQuitRoom);
			}
		}
		gameServer.sendMsg(cmd);
	}

	public void processUserNotReqQuitRoom(User user) {
		SendNotifyNotReqQuitRoom cmd = new SendNotifyNotReqQuitRoom();
		cmd.chair = user.getPlayerId() - 1;
		cmd.reason = SendNotifyReqQuitRoom.REQ_SUCCESS;
		if (gameServer.getServerState() == GameServer.gsNoPlay || user.getJoinedRoom() == null) {
			// if (gameState == gmNoPlay || user.getJoinedRoom() == null) {
			cmd.reason = SendNotifyReqQuitRoom.REQ_FAIL;
		}
		if (user.getPlayerId() >= 1 && user.getPlayerId() <= ServerConstant.MAX_PLAYER) {
			GamePlayer gp = gameServer.getPlayerByChair(user.getPlayerId());
			if (gp != null && gp.getPlayerStatus() == GamePlayer.psPLAY) {
				gp.reqQuitRoom = false;
				gp.timeDown = ServerConstant.USER_TIME_OUT;
			}
		}
		gameServer.sendMsg(cmd);
	}

	public User getUser(int uId) {
		return ExtensionUtility.globalUserManager.getUserById(uId);
	}

	/*
	 * public PlayerInfo getPlayerInfo(int uId) { return
	 * getExtension().getPlayerInfo(uId) ; }
	 */

	/*
	 * private void TinhDiemExp(int i) { UserScore expScore = new UserScore();
	 * expScore.exp = updateLevel(i); GamePlayer gp =
	 * gameServer.getPlayerByChair(i); if (gp != null && gp.isPlaying) {
	 * PlayerInfo pInfo = gp.getPlayingInfo(); //User user =
	 * getUser(PlayerUid[i]); if (pInfo != null) { //long nMoney =
	 * BinhExtension.instance().checkLevelUp(gp.getUser(), pInfo.exp, pInfo.exp
	 * + expScore.exp); long nMoney = getExtension().checkLevelUp(gp.getUser(),
	 * pInfo.exp, pInfo.exp + expScore.exp); if (nMoney > 0) { expScore.bean =
	 * nMoney; MetricLog.writeLevelUp((int)gp.sInfo.uId, gp.sInfo.device,
	 * nMoney, gameServer.getRoomId(), pInfo.bean, pInfo.coin, pInfo.exp); }
	 * getExtension().setUserScore((int) gp.sInfo.uId, expScore, true); } } }
	 */
	public void processUserMaximize(User user, ReceiveMaximize rcv) {
		PlayerInfo pInfo = (PlayerInfo) user.getProperty(ServerConstant.PLAYER_INFO);
		if (pInfo != null) {
			if (rcv.maximize)
				MetricLog.writeMaximizer(user, pInfo.bean, pInfo.coin, 1);
			else
				MetricLog.writeMaximizer(user, pInfo.bean, pInfo.coin, 2);
		}
	}

	public int infoTableReconnect(User u, GamePlayer gamePlayer) {

		// Debug.trace("THONG TIN BAN CHOI KHI RECONNECT");
		if (u == null || gamePlayer == null) {
			Debug.trace("Reconnect error ==>" + u + "|" + gamePlayer);
			return -22;
		}
		PlayerInfo pInfo = (PlayerInfo) u.getProperty(ServerConstant.PLAYER_INFO);
		if (pInfo == null) {
			Debug.trace("Reconnect error pinfo null");
			return -23;
		}
		Room r = gameServer.getRoom();
		if (r == null) {
			Debug.trace("Reconnect error room null");
			return -24;
		}
		if (gamePlayer.getPlayerStatus() != GamePlayer.psPLAY) {
			gameServer.kickUserFromRoom(gamePlayer.user, SendKickFromRoom.Reason.eKICK_REASON_GAME_ERROR,
					gamePlayer.getPlayerId());
			Debug.trace("Reconnect error playerstatus != psPlay");
			return -25;
		}
		// gamePlayer.nAfk = 0;
		byte myChair = (byte) (gamePlayer.getPlayerId() - 1);
		gamePlayer.takeChair(u, gamePlayer.getPlayerId(), roomInfo.minBet);
		Debug.trace("GameManager::infoTableReconnect: " + u.getPlayerId());

		gamePlayer.setPlayerStatus(GamePlayer.psPLAY);

		int respon = -1;
		try {
			respon = ExtensionUtility.instance().joinRoom(u, r, r.getPassword(), false);
		} catch (Exception e) {
			CommonHandle.writeErrLog(e);
		}
		if (respon != 0) {
			Debug.trace("Rejoin Room :" + u.getId() + " room:" + r.getId() + " - response:" + respon);
			return -26;
		}
		//
		SendInfotableReconnect cmd = new SendInfotableReconnect();

		cmd.roomBet = moneyBet;
		cmd.roomId = r.getId() + 1;
		cmd.currentTime = (byte) gameTime;
		cmd.gameState = (byte) gameServer.getServerState();
		// cmd.gameState = (byte) gameState;
		cmd.comission = 100;// Game 3 cay ko can dung cai nay
		cmd.myChair = myChair;
		cmd.dealer = (byte) (roomInfo.dealerPlayerId - 1);

		cmd.betExtra = roomInfo.betExtraList;

		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = gameServer.getPlayerByChair(i);
			cmd.playerStatus[i - 1] = (byte) gp.getPlayerStatus();
			cmd.players[i - 1] = gp.getPlayerInfo();
			cmd.goldTemp[i - 1] = gp.getGoldTemp();
			if (gp.getUserId() == gameServer.getRoom().getId()) {
				cmd.roomOwner = (byte) (i - 1);
			}
		}

		List<DeckInfo> deckInfos = new ArrayList<DeckInfo>();
		GamePlayer owner = gameServer.getPlayerByChair(myChair + 1);
		for (int i = 0; i < gameServer.getPlayerReadyList().size(); i++) {
			GamePlayer player = gameServer.getPlayerReadyList().get(i);

			DeckInfo info = new DeckInfo();
			info.chair = (byte) (player.getPlayerId() - 1);
			info.isShowCard = player.isShowCard;
			info.cardIds = player.myCards.getMyCardID();

			info.isBet = player.isBet();
			info.goldBet = player.getBet();

			info.isChickedJoined = player.isChickenJoined;

			if (player == owner) {
				info.betExtraInfo.status = BetExtraInfo.STATUS_NONE;
			} else {
				info.betExtraInfo.status = owner.getBetExtraStatus(player);
				if (info.betExtraInfo.status == BetExtraInfo.STATUS_ACCEPT) {
					info.betExtraInfo.betIdx = owner.betExtraList.get(player);
				} else if (info.betExtraInfo.status == BetExtraInfo.STATUS_RECEIVE) {
					info.betExtraInfo.betIdx = player.getBetExtraIdxFromInvitation(owner);
				} else if (info.betExtraInfo.status == BetExtraInfo.STATUS_SENT) {
					info.betExtraInfo.betIdx = owner.getBetExtraIdxFromInvitation(player);
				}
			}

			deckInfos.add(info);
		}

		cmd.deckInfos = deckInfos;
		cmd.myCard = owner.myCards.getMyCardID();
		cmd.goldInChicken = gameServer.playersChickenJoined.size() * getBetInChicken();
		cmd.turnTime = timeCountDown;
		cmd.turnType = (byte) curTurnType;

		gameServer.sendMsg(cmd, u);

		SendRoomInfo msg = new SendRoomInfo();
		msg.roomInfo = roomInfo;
		gameServer.sendMsg(msg, u);
		return 0;
	}

	public void setCuoclon(boolean b) {
		this.cuoclon = b;
	}

	public boolean isCuoclon() {
		return this.cuoclon;
	}

	public void setAutoStartCountdown(int num) {
		this.timeCountDown = num;
	}

	public void shuffleCard() {
		List<Integer> cardList = new ArrayList<Integer>(ServerConstant.MAX_CARDS);
		for (int i = 0; i < ServerConstant.MAX_CARDS; i++) {
			cardList.add(i);
		}

		if (cardID == null) {
			cardID = new int[ServerConstant.MAX_CARDS];
		}

		int count = 0;
		while (!cardList.isEmpty()) {
			int ranIdx = new Random().nextInt(cardList.size());
			cardID[count] = cardList.get(ranIdx);
			cardList.remove(ranIdx);
			count++;
		}

		// for (int i = 0; i < cardID.length; i++) {
		// Debug.trace("GameManager::shuffleCard: " + cardID[i]);
		// }
	}

	private void dealTheCard(List<GamePlayer> players, int numCard) {
		int count = 0;

		if (isConfigCard) {
			for (int i = 0; i < players.size(); i++) {
				for (int j = 0; j < numCard; j++) {
					GamePlayer gamePlayer = players.get(i);
					gamePlayer.myCards.receiveCard(new Card(ConfigCards[count]), j);
					count++;
				}
			}
		} else {
			for (int j = 0; j < numCard; j++) {
				for (int i = 0; i < players.size(); i++) {
					GamePlayer gamePlayer = players.get(i);
					gamePlayer.myCards.receiveCard(new Card(cardID[count]), j);
					count++;
				}
			}
		}

		for (int i = 0; i < players.size(); i++) {
			GamePlayer gamePlayer = players.get(i);
			gamePlayer.myCards.sortCard();
//			Debug.info("GameManager::dealTheCard: room: " + gameServer.getRoom().getId() + " uid "
//					+ gamePlayer.getpInfo().getUId() + " -> " + gamePlayer.myCards.getName());
			if (numCard == ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME && gamePlayer.myCards.isSapARo()) {
				MetricLog.bacaySapARo(gamePlayer.getpInfo().getUId(),gamePlayer.myCards.getHandCard(),this.getMoneyBet());
			}
		}

		if (numCard == 3) {
			try {
				int ran = new Random().nextInt(100);
				if (ran <= WIN_RATE && WIN_RATE > 0) {
					GamePlayer winner = null;
					for (int i = 0; i < players.size(); i++) {
						if (winner == null) {
							winner = players.get(i);
							continue;
						}
						GamePlayer gamePlayer = players.get(i);
						if (gamePlayer.myCards.compare(winner.myCards)) {
							winner = gamePlayer;
						}
					}

					GamePlayer dealer = gameServer.getPlayerByChair(roomInfo.dealerPlayerId);
					if (dealer.isBot()) {
						swapCard(dealer, winner);
					} else {
						List<GamePlayer> bots = new ArrayList<>();
						for (int i = 0; i < players.size(); i++) {
							GamePlayer gamePlayer = players.get(i);
							if (gamePlayer.isBot()) {
								bots.add(gamePlayer);
							}
						}

						if (bots.size() != 0) {
							GamePlayer bot = bots.get(new Random().nextInt(bots.size()));
							swapCard(winner, bot);
						}
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void swapCard(GamePlayer player1, GamePlayer player2) {
		Debug.trace(
				"GameManager::swapCard: before " + player1.getPlayerInfo().getUId() + ": " + player1.myCards.getName()
						+ "   " + player2.getPlayerInfo().getUId() + ": " + player2.myCards.getName());
		PlayerCard temp = player1.myCards;
		player1.myCards = player2.myCards;
		player2.myCards = temp;
		Debug.trace(
				"GameManager::swapCard: after " + player1.getPlayerInfo().getUId() + ": " + player1.myCards.getName()
						+ "   " + player2.getPlayerInfo().getUId() + ": " + player2.myCards.getName());
	}

	public long getBetInChicken() {
		return roomInfo.chickenBet;
	}

	public boolean isTurn(int turnType) {
		return curTurnType == turnType;
	}

	public void setTurn(int turnType) {
		Debug.trace("GameManager::setTurn: curTurn-Time" + curTurnType + " -> " + timeCountDown + " setTo: " + turnType
				+ " -> " + mapTurnType.get(turnType));
		curTurnType = turnType;
		timeCountDown = mapTurnType.get(turnType);

	}

	public long getGoldBetExtra(int betIdx) {
		return roomInfo.betExtraList[betIdx];

	}

	public int getFactor(PlayerCard playerCard) {
		if (playerCard.isSap())
			return roomInfo.hsSap;
		if (playerCard.getScore() == 10)
			return roomInfo.hs10;

		return 1;
	}

	// thuong sua o day : Neu gold_bot > random(800k-1M) hoac room full --> kick
	private void kickBot() {
		int n = 0;
		int k = 0;
		int idx = 0;
		boolean isMaxGold = false;
		int maxGold = Utils.randomInRange(800000, 1000000);
		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = gameServer.getPlayerByChair(i);
			if (gp != null) {
				if (gp.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
					n++;
					PlayerInfo pInfo = gp.getPlayerInfo();
					if (pInfo != null) {
						if (pInfo.isBot()) {
							k++;
							idx = i;
							if (pInfo.bean >= maxGold) {
								isMaxGold = true;
							}
						}
					}
				}
			}
		}
		if ((n == ServerConstant.MAX_PLAYER && k == 1) || isMaxGold) {
			GamePlayer gp = gameServer.getPlayerByChair(idx);
			if (gp != null && gp.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
				PlayerInfo pInfo = gp.getPlayerInfo();
				if (pInfo != null && pInfo.isBot()) {
					gp.reqQuitRoom = true;
					Debug.info("Kick BOT:" + pInfo.getUId() + "|" + pInfo.bean);
				}
			}
		}
	}
}
