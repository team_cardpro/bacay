package game.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import game.modules.BaseEvent.hu.HuConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import game.BaCayExtension;
import game.GameType;
import game.entities.BetExtraInfo;
import game.entities.BotConstants;
import game.entities.GoldMgr;
import game.entities.PlayerInfo;
import game.entities.RoomInfo;
import game.entities.ServerConstant;
import game.entities.TurnType;
import game.entities.UserScore;
import game.modules.bot.BotManager;
import game.server.BaCayLogic.PlayerCard;
import game.server.BaCayLogic.sPlayerInfo;
import game.server.cmd.SendKickFromRoom;
import game.utils.MetricLog;
import game.utils.Utils;


public class GamePlayer extends BaseGamePlayer{
    public static final int psNO_LOGIN = 0;
    public static final int psSIT = 1;
    public static final int psREADY = 2;
    public static final int psPLAY = 3;
    public static final int psWAIT = 4;
    public static final int psLOOKON = 5;
    public static final int psWATCH = 6;

    private volatile int _pStatus = psNO_LOGIN;
    public volatile User user;
    private volatile long userId = -1;
    public sPlayerInfo sInfo = new sPlayerInfo();
    public volatile boolean connect;
    public volatile PlayerCard myCards;
    //public boolean EarlyQuit;
    public volatile boolean isPlaying;
    public volatile boolean isReady; //so sanh so bai
    public volatile boolean isSelectCard; //so sanh so bai


    //public volatile boolean IsRequestBaseInfo;

    public volatile boolean reqQuitRoom = false; //dang ky thoat game sau van dau

    //public volatile int nKick = 2;

    public volatile int timeDown = ServerConstant.USER_TIME_OUT;

    private volatile int roomType = ServerConstant.TABLE_NEWBIE;

    private final Logger logger = LoggerFactory.getLogger("BaCayLogic");

    private volatile int fakeId = -1;

    //bot
    public short countGameContinue = 0;
    public short maxGameContinue = 0;

    //Tung
    private volatile int playerId = -1;

    public boolean isShowCard = false;
    public boolean isChickenJoined = false;
    private long bet = 0;
    private boolean isBet = false;

    private int afkCounter = 0;

    private final Map<GamePlayer, Integer> betExtraInvitationList = new HashMap<GamePlayer, Integer>();//Danh sach nhung thang danh bien voi ng choi nay
    public final Map<GamePlayer, Integer> betExtraList = new HashMap<GamePlayer, Integer>();//Danh sach nhung thang danh bien voi ng choi nay


    public static final int REASON_BET = 1;//dat vs cai - tien
    public static final int REASON_BET_EXTRA = 2;//danh bien - tien
    public static final int REASON_BET_CHICKEN = 3;//dat ga - tien
    public static final int REASON_END_BET = 4;//end so vs cai + tien neu thang
    public static final int REASON_END_BET_EXTRA = 5;//end danh bien + tien neu thang
    public static final int REASON_END_BET_CHICKEN = 6;//end ga 
    public static final int REASON_BACK_BET = 7;//tra tien cuoc cai
    public static final int REASON_BACK_BET_EXTRA = 8;//tra lai tien cuoc bien
    public static final int REASON_X_SCORE = 9;
    private GameServer gameServer;
    public int roomBet;

    private long goldTemp = 0;//Tien duoc luu tam de +- cuoi game
    private long goldWin = 0;//Dung de compare voi goldTemp. goldTemp != goldWin -> SAI
    private long metricGoldCms = 0;
    private long metricGoldBetTotal = 0;
    public long metricGoldKept = 0;
    //Tung end

    private void trace(String s) {
        logger.info("{GamePlayer}: " + this.toString() + "====>" + s);
    }

    @Override
    public String toString() {
        return "GamePlayer [_pStatus=" + _pStatus + ", userId=" + userId + ", connect=" + connect + ", isPlaying="
                + isPlaying + ", timeDown=" + timeDown + ", timeCowndown=" + timeCowndown + "]";
    }


    public GamePlayer() {
        super();
        setPlayerStatus(psNO_LOGIN);
        myCards = new PlayerCard();
        connect = false;
    }

    public synchronized void login(User pUser, int minBet) {
        // if(checkExits(pUser)) return;
        user = pUser;
        trace("Login");

        setPlayerStatus(psSIT);
        pInfo = (PlayerInfo) user.getProperty(ServerConstant.PLAYER_INFO);
        pInfo.setIsPlaying(ServerConstant.GAME_ID);
//        PlayerInfo.saveToDB(pInfo);
        userId = pUser.getId();
        //nKick = 2;
        reqQuitRoom = false;
        timeDown = ServerConstant.USER_TIME_OUT;
        countGameContinue = 0;
        if (pInfo != null && pInfo.isBot()) {
            maxGameContinue = (short) Utils.randomInRange(BotConstants.MIN_GAME_REMAIN, BotConstants.MAX_GAME_REMAIN);
        }
        connect = false;

        //Tung
        setPlayerId(user.getPlayerId());
        resetAfk();

        roomBet = minBet;
    }

    public synchronized void loginViewer(User pUser, int minBet) {
        //if(checkExits(pUser)) return;
        user = pUser;
        trace("loginViewer: ");
        setPlayerId(-1);
        setPlayerStatus(psLOOKON);
        pInfo = (PlayerInfo) pUser.getProperty(ServerConstant.PLAYER_INFO);
        pInfo.setIsPlaying(GameType.NO_PLAYING);
//        PlayerInfo.saveToDB(pInfo);
        //nKick = 2;
        userId = pUser.getId();
        connect = false;

        roomBet = minBet;

        resetAfk();

        if (pInfo != null && pInfo.isBot()) {
            maxGameContinue = (short) Utils.randomInRange(BotConstants.MIN_GAME_REMAIN, BotConstants.MAX_GAME_REMAIN);
        }


    }

    public synchronized void logout() {
        
    	if(pInfo != null && pInfo.getIsPlaying() == ServerConstant.GAME_ID)
            pInfo.setIsPlaying(GameType.NO_PLAYING);            
        if (pInfo != null && pInfo.isBot()) {
			BotManager.getInstance().addUid(pInfo.getUId());
		}
        if (getUser() != null) {
			getUser().removeProperty(ServerConstant.PLAYER_CHAIR);
		}
    	
        setPlayerStatus(psNO_LOGIN);
        reqQuitRoom = false;
        userId = -1;
        pInfo = null;
        user = null;
        setPlayerId(-1);
        countGameContinue = 0;
        maxGameContinue = 0;
        connect = false;


        gameServer = null;
    }


    public synchronized void clearInfo() {
        if (getUser() != null) {
            pInfo = (PlayerInfo) getUser().getProperty(ServerConstant.PLAYER_INFO);
            if (pInfo.getIsPlaying() == ServerConstant.GAME_ID)
                pInfo.setIsPlaying(GameType.NO_PLAYING);
        }
        userId = -1;
        setPlayerStatus(psNO_LOGIN);
        pInfo = null;
        user = null;
        setPlayerId(-1);
        reqQuitRoom = false;
        connect = false;
    }

    /**
     * set/get tr?ng th�i c?a ng�?i ch�i
     *
     * @return
     */
    public synchronized int getPlayerStatus() {
        return _pStatus;
    }

    public synchronized void setPlayerStatus(int status) {
        _pStatus = status;
//        Debug.trace("GamePlayer::setPlayerStatus: " + status);
        trace("setPlayerStatus");
    }

    private BaCayExtension getExtension() {
        return (BaCayExtension) ExtensionUtility.getExtension();
    }

    public PlayerInfo getPlayingInfo() {
        return getExtension().getPlayerInfo((int) sInfo.uId);
        //return pInfo;
    }

    public PlayerInfo getPlayerInfo() {
        return pInfo;
        //return BinhExtension.instance().getPlayerInfo((int)userId);
        //return getExtension().getPlayerInfo((int) userId);
    }

    public synchronized int getUserId() {
        return (int) userId;
    }


    public void setInfo(PlayerInfo pInfo) {
        sInfo.bean = pInfo.bean;
        sInfo.coin = pInfo.coin;
        sInfo.lostCount = pInfo.lostCount;
        sInfo.score = pInfo.score;
        sInfo.uId = pInfo.getUId();
        sInfo.name = pInfo.displayName;
        sInfo.winCount = pInfo.winCount;
        User u = getUser();
        if (u != null)
            sInfo.device = ((Integer) u.getProperty(ServerConstant.DEVICE_ID)).intValue();
    }


    public void Reset() {
    	Debug.trace("GamePlayer reset Uid = " + sInfo.uId);
        sInfo.Reset();
        isReady = false;
        isPlaying = false;
        isBet = false;
        isShowCard = false;
        isChickenJoined = false;
    }

    public User getUser() {
        User ret = ExtensionUtility.globalUserManager.getUserById((int) userId);
        return ret;
    }
  
    /*public void setUser(User user)
    {
      this.user = user;
    }*/

    public void startGame(int rType) {
        roomType = rType;
        isPlaying = true;
        isReady = false;
        myCards.roomType = rType;
        myCards.removeAllCard();
        timeCowndown = 0;
        if (pInfo != null) {
            if (pInfo.isBot()) {
                timeCowndown = Utils.randomInRange(5, 20);
            }
        }
        //bot
        countGameContinue++;
        if (pInfo != null && pInfo.isBot() && countGameContinue >= maxGameContinue) {
            reqQuitRoom = true;
            Debug.trace("Bot req quit:" + countGameContinue  + "|" + maxGameContinue);
        }
    }

    private int timeCowndown = 0;
    private int turnType;

    public void onLoop() {
        if (timeCowndown > 0) {
            timeCowndown--;
            if (timeCowndown == 0) {
                switch (turnType){
                    case TurnType.BOT_BET:
                        doBet();
                        break;
                    case TurnType.BOT_JOIN_CHICKEN:
                        doJoinChicken();
                        break;
                    case TurnType.BOT_SHOW_CARD:
                        doShowCard();
                        break;
//                    case TurnType.BET_START:
//                        doBet();
//                        break;
                }
            }
        }
    }

    public boolean isLogin() {
        return _pStatus > psNO_LOGIN;
    }

    
    public void takeChair(User u, int playerId, int minBet) {
        user = u;

        pInfo = (PlayerInfo) user.getProperty(ServerConstant.PLAYER_INFO);
        pInfo.setIsPlaying(ServerConstant.GAME_ID);
        userId = u.getId();
        connect = true;

        //Tung
        setPlayerId(playerId);
        resetAfk();

        roomBet = minBet;
    }

    public boolean checkGold(long gold) {
        return ((getGold() + goldTemp) >= gold);
    }
    public boolean checkGold(long gold,int bean) {
        return ((bean + goldTemp) >= gold);
    }

    public void setBet(long bet, int roomId) {
        this.bet = bet;
        this.isBet = true;
        PlayerInfo.resetGoldKept(getUserId());
        PlayerInfo.resetGoldKeptReal(getUserId());
        addGold(roomId, -bet, GamePlayer.REASON_BET,0);
    }

    public long getBet() {
        return bet;
    }

    public boolean isBet() {
        return isBet;
    }

    public void resetGame() {
        PlayerInfo.setIsHold((int) sInfo.uId, false);
        _pStatus = psREADY;
        this.isBet = false;
        this.isShowCard = false;
        this.isChickenJoined = false;
        betExtraInvitationList.clear();
        betExtraList.clear();
        setPlayerId(user.getPlayerId());
    }

    public boolean isExistInvitation(GamePlayer player) {
        return betExtraInvitationList.containsKey(player);
    }

    public void inviteBetExtra(GamePlayer player, int betType) {
        betExtraInvitationList.put(player, betType);
    }


    public void responseBetExtra(GamePlayer player, boolean isAccept, int betIdx) {
//        if (isExistInvitation(player))
        {
            if (isAccept) {
                betExtraList.put(player, betIdx);
                betExtraInvitationList.remove(player);
            } else {
                betExtraInvitationList.remove(player);
            }
        }
    }

    public int getBetExtraIdxFromInvitation(GamePlayer player) {
        if (isExistInvitation(player))
            return betExtraInvitationList.get(player);
        return -1;
    }

    public List<GamePlayer> getBetExtraList() {
        return new ArrayList<GamePlayer>(betExtraList.keySet());
    }

    public long getGold() {
        PlayerInfo pInfo = this.getPlayerInfo();
        if (pInfo != null && pInfo.isBot()) {
            return pInfo.bean;
        }
       
        int tmpUid = -1;
        
        
        if(pInfo != null){
        	Debug.trace("GamePlayer Player status uid = " + pInfo.getUId() + "|" + getPlayerStatus());
        }
        else{
        	Debug.trace("GamePlayer pInfo is null :" + this.getUserId() +"|"+ this.playerId);
        }
        
        if(sInfo.uId == -1 && pInfo != null){//sInfo.uid = -1
        	
        	Debug.trace("GamePlayer Player status pInfo uid = " + pInfo.getUId());
        	
        	tmpUid = pInfo.getUId();
        }
        else{
        	Debug.trace("GamePlayer Player status sInfo uid = " + this.sInfo.uId);
        	
        	tmpUid =  (int) this.sInfo.uId;
        }
        
        Debug.trace("GamePlayer Player status uid = " + tmpUid);
        
//        long ret = 
        Integer ret = GoldMgr.get(tmpUid);
        
        Debug.trace("GamePlayer Player ret uid = " + tmpUid + "|" + ret);
        
        return ret;
    }

    public void addGold(int roomId, long gold, int reason,int fee) {
        addGold(roomId, gold, reason, "",fee);

    }

    public void addGold(int roomId, long gold, int reason, String ext,int fee) {
    	if(getpInfo().getUId() == 0 && gameServer != null){
            gameServer.destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
            return;
        }

        updateGoldKept(gold, reason);
        MetricLog.updateGoldAction(getpInfo(), reason, gold, getGold(), roomBet, roomId, PlayerInfo.getGoldKept( sInfo.uId), gameServer.getGameManager().codeVanChoi,fee);
    }

    public void updateGoldChange()
    {
        if (isBot()){
            if (pInfo != null && pInfo.isBot()) {
                pInfo.bean += goldTemp;
            }
        }
        else {
            UserScore score = new UserScore();
            score.bean = goldTemp;
            getExtension().setUserScore((int) this.sInfo.uId, score, true);
        }
        writeMetric(goldTemp);
        
        goldTemp = 0;
        PlayerInfo.resetGoldKeptToDefault( sInfo.uId, -gameServer.getGameManager().roomInfo.goldPlayer);
        PlayerInfo.resetGoldKeptRealToDefault(sInfo.uId, gameServer.getGameManager().roomInfo.goldPlayer);
    }
    
    public boolean canCalculate(){
    	if (BotManager.isBot(pInfo.getUId())) {
			return true;
		}
    	long curGold = PlayerInfo.getGold(pInfo.getUId());
    	return curGold + goldTemp >= 0;
    }

    private void updateGoldKept(long gold, int reason) {
        goldTemp += gold;

        int hs = 1;
        if (reason == REASON_BET){
            hs = gameServer.getGameManager().roomInfo.hs10;
            PlayerInfo.updateGoldKeptReal(sInfo.uId, -gold);
        }


        PlayerInfo.updateGoldKept( sInfo.uId, gold * hs);
//

    }

    public void updateGoldWin(long goldWin, int goldCms){
        this.goldWin += goldWin;
        this.metricGoldCms += goldCms;
        if (goldCms > 0){
            int goldHu = (int) (goldCms * 0.2);
            gameServer.SendGopHu(goldHu, gameServer.getRoomId(), (int) gameServer.getMoneyBet(), HuConstant.Hu_Reason.GOP_PHE_HU);
            Debug.trace("GamePlayer::updateGoldWin: " + goldWin + " goldCms: " + goldCms + " goldHu: " + goldHu);
        }
    }

    public void writeMetric(long gold)
    {
//        if (isBot())
//            return;
        if (gold != goldWin){
            Debug.trace("GamePlayer::writeMetric: SAIIIIIIIII");
        }
        long goldStart = getGold();
        long goldEnd = goldStart + gold;

        int roomId = -1;
        if (gameServer.getRoom() != null){
            roomId = gameServer.getRoom().getId();
        }

        GamePlayer player = gameServer.getPlayerByChair(gameServer.getGameManager().roomInfo.dealerPlayerId);
        int dealerId = -1;
        if (player != null){
            dealerId = player.getUserId();
        }
        MetricLog.logEndGame(getpInfo(), goldStart, PlayerInfo.getGold(getUserId()), gold, metricGoldCms, gameServer.playerList, 
        		roomId, gameServer.getGameManager().roomInfo.minBet, metricGoldKept, metricGoldBetTotal, dealerId,getRemainCard(),gameServer.getGameManager().codeVanChoi);
//        MetricLog.writeLogUpdateGoldPerGame(user, goldStart, goldEnd, gold, metricGoldCms, gameServer.playerList,
//                    roomId, gameServer.getGameManager().roomInfo.minBet, metricGoldKept, metricGoldBetTotal, dealerId,getRemainCard());

    }

    private String getRemainCard() {
    	if (myCards != null) {
    		return myCards.getHandCard();
		}
		return "";
	}

	public byte getBetExtraStatus(GamePlayer player) {
        byte ret = BetExtraInfo.STATUS_NONE;
        if (betExtraList.containsKey(player)) {
            ret = BetExtraInfo.STATUS_ACCEPT;
        } else if (isExistInvitation(player)) {
            ret = BetExtraInfo.STATUS_SENT;
        } else if (player.isExistInvitation(this)) {
            ret = BetExtraInfo.STATUS_RECEIVE;
        }
        return ret;
    }


    public final int getPlayerId() {
        if (playerId <= 0) {
            if (user != null)
                Debug.trace("GamePlayer::getPlayerId: " + user.getId());
        }
        return playerId;
    }

    public void resetAfk() {
        afkCounter = 0;
    }

    public void updateAfk() {
        afkCounter++;
    }

    public boolean isAfk() {
        return afkCounter >= ServerConstant.NUM_AFK;
    }

    public void setPlayerId(int playerId) {
        if (user != null)
            Debug.trace("GamePlayer::setPlayerId: " + user.getId() + " id: " + playerId);
        {
            Debug.trace("GamePlayer::setPlayerId: NULLLLLLL " + playerId);
        }
        Debug.trace("GamePlayer::setPlayerId: " + playerId);
        this.playerId = playerId;
    }

    public void setGameServer(GameServer gameServer) {
        this.gameServer = gameServer;
    }

    public void betStart(int turnTime){
        goldTemp = 0;
        metricGoldCms = 0;
        goldWin = 0;
        metricGoldBetTotal = 0;
        PlayerInfo.resetGoldKeptToDefault( sInfo.uId, -gameServer.getGameManager().roomInfo.goldPlayer);
        PlayerInfo.resetGoldKeptRealToDefault(sInfo.uId, gameServer.getGameManager().roomInfo.goldPlayer);



        if (isBot()) {
            timeCowndown = Utils.randomInRange(1, turnTime/2);
            turnType = TurnType.BOT_BET;
        }

    }

    public void showCardStart(int turnTime){
        if (isBot()){
            timeCowndown = Utils.randomInRange(1, turnTime);
            turnType = TurnType.BOT_SHOW_CARD;
        }
    }

    private void doBet(){
        RoomInfo roomInfo = gameServer.getGameManager().roomInfo;
        int minBet = roomInfo.minBet;
        int maxBet = (int) Math.min(getGold()/2, roomInfo.curMaxBet);
        int avg = (roomInfo.maxBet - roomInfo.minBet) / 19;
        int ran = new Random().nextInt(20);

        int goldBet = Math.min(maxBet, ran * avg);

        gameServer.processPlayerBet(user, goldBet);

        boolean joinChicken = new Random().nextBoolean();
        if (joinChicken){
            timeCowndown = new Random().nextInt(3);
            turnType = TurnType.BOT_JOIN_CHICKEN;
        }
    }

    private void doJoinChicken(){
        gameServer.processPlayerJoinChicken(user);
        doInviteBetExtra();
    }

    private void doInviteBetExtra(){
        RoomInfo roomInfo = gameServer.getGameManager().roomInfo;
        for(GamePlayer gamePlayer : gameServer.getPlayerReadyList()){
            if(gamePlayer.getPlayerId() == this.getPlayerId()
                    || gamePlayer.getPlayerId() == roomInfo.dealerPlayerId){
                continue;
            }
            boolean ran = new Random().nextBoolean();
            if (!ran)
                continue;
            int betIdx = new Random().nextInt(roomInfo.betExtraList.length);
            gameServer.processInviteBetExtra(user, betIdx, (byte) (gamePlayer.getPlayerId() - 1));
        }
    }

    private void doShowCard(){
        gameServer.processPlayerShowCard(user);
    }

    public boolean isBot() {
        if (getPlayerInfo() == null)
            return false;
        return getPlayerInfo().isBot();
    }

    public long getGoldTemp() {
        return goldTemp;
    }
}
