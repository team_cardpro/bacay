package game.server.BaCayLogic;

import bitzero.util.common.business.Debug;

public class Card {

    public static final int ES_HEART = 4;
    public static final int ES_DIAMOND = 3;
    public static final int ES_CLUB = 2;
    public static final int ES_SPADE = 1;

    public static final int EC_RED = 0;
    public static final int EC_BLACK = 1;
    public static final int EC_UNDEFINED = 2;

    public int ID;
    //public int eCardState;
    public int auxiVal;

    //Át = 1;
    
    public Card() {
        super();
    }

    public Card(int id) {
        super();
        ID = id;
        Debug.trace("Card::Card " + id + ": " + getName());
    }

    public int getNumber() {
        return (ID / 4 + 1);
    }

    public int getSuit() {
        return (ID % 4 + 1);
    }

    public String getName(){
        String ret = "";

        String shape = "";
        switch (getSuit()) {
            case 1:
                shape = " Bich";
                break;
            case 2:
                shape = " Tep";
                break;
            case 3:
                shape = " Co";
                break;
            case 4:
                shape = " Ro";
                break;
        }
        ret = getNumber() + shape;
        return ret;
    }

    public int GetColor() {
        //Convert code Binh
        if (getSuit() == Card.ES_CLUB || getSuit() == Card.ES_SPADE)
            return Card.EC_BLACK;
        if (getSuit() == Card.ES_DIAMOND || getSuit() == Card.ES_HEART)
            return Card.EC_RED;
        return Card.EC_UNDEFINED;
    }

    //true -> card1 lon hon, false -> card2 lon hon
    static public boolean compare(Card card1, Card card2)
    {
        if (card1.isAtRo())
            return true;
        if (card2.isAtRo())
            return false;
        if (card1.getSuit() > card2.getSuit())
            return true;
        if (card1.getSuit() == card2.getSuit() && card1.getNumber() > card2.getNumber())
            return true;
        return false;
    }

    public boolean compare(Card card)
    {
        return compare(this, card);
    }

    public boolean isAtRo(){
        if (getNumber() == 1 && getSuit() == Card.ES_HEART)
            return true;
        return false;
    }

	@Override
	public String toString() {
		return getName();
	}

    
}
