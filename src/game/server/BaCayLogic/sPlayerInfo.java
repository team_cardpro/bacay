package game.server.BaCayLogic;

public class sPlayerInfo {
    public long bean;
    public long uId;
    public long coin;
    public long score;
    public long exp;
    public String name;
    public int device;
    public int winCount;
    public int lostCount;
    public int dogfall;
    public int escapeCount;

    public sPlayerInfo() {
        super();
    }

    public void Reset() {
        bean = 0;
        uId = -1;
        coin = 0;
        score = 0;
        exp = 0;
        name = "";
        device = 0;
        winCount = 0;
        lostCount = 0;
        dogfall = 0;
        escapeCount = 0;
    }
}
