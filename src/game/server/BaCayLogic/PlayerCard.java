package game.server.BaCayLogic;

/*import java.util.ArrayList;
import java.util.LinkedList;*/
//import game.Card;

import game.entities.ServerConstant;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bitzero.util.common.business.Debug;


public class PlayerCard {

    public Card[] myCard = new Card[ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME];
    public Card maxCard = null;//Quan bai lon nhat
    public int roomType = ServerConstant.TABLE_NEWBIE;


    private static Logger logger = LoggerFactory.getLogger("BaCayLogic");

    private static void trace(String s) {
        logger.info("{PlayerCard}: " + s);
    }

    public PlayerCard() {
        super();

    }

    public void receiveCard(Card card, int idx) {
        myCard[idx] = card;
    }

    public synchronized void removeAllCard() {
        myCard = new Card[ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME];
    }

    public void sortCard() {
        maxCard = null;
        if(myCard.length > 0){
        	maxCard = myCard[0];
        	if (maxCard.isAtRo()) {
				return;
			}else{
				for (int i = 1; i < myCard.length; i++) {
					if (myCard[i] == null) {
						continue;
					}
					if (myCard[i].compare(maxCard)) {
						maxCard = myCard[i];
					}
				}
			}
        }
        //code cu
//        for (int i = 0; i < myCard.length; i++){
//            if (myCard[i] == null){
//                continue;
//            }
//            if (maxCard == null){
//                maxCard = myCard[i];
//                continue;
//            }
//            if (myCard[i].isAtRo()){
//                //At Ro
//                maxCard = myCard[i];
//                break;
//            }
//
//            if (myCard[i].getSuit() > maxCard.getSuit()){
//                maxCard = myCard[i];
//            }
//            else if (myCard[i].getSuit() == maxCard.getSuit()){
//                if (myCard[i].getNumber() > maxCard.getNumber()){
//                    maxCard = myCard[i];
//                }
//            }
//        }
    }

    public int getScore() {
        int total = 0;
        for (int i = 0; i < myCard.length; i++) {
            if (myCard[i] == null)
                continue;
            total += myCard[i].getNumber();
        }

        int ret = total % 10;
        ret = ret == 0 ? 10 : ret;
        return ret;
    }

    public byte[] getMyCardID() {
        byte[] ret = new byte[ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME];
        for (int i = 0; i < myCard.length; i++) {
            if (myCard[i] != null) {
                ret[i] = (byte) myCard[i].ID;
            }
        }
        return ret;
    }


    public int[] getMyCardIDInt() {
        int[] ret = new int[ServerConstant.MAX_CARD_PER_PLAYER_IN_GAME];
        for (int i = 0; i < myCard.length; i++) {
            if (myCard[i] != null) {
                ret[i] = (byte) myCard[i].ID;
            }
        }
        return ret;
    }

    public boolean compare(PlayerCard cards)
    {
        if (this.isSap() && cards.isSap())
        {
            int myNum = maxCard.getNumber() == 1 ? 10 : maxCard.getNumber();
            int oppNum = cards.maxCard.getNumber() == 1 ? 10 : cards.maxCard.getNumber();
            return myNum > oppNum;
        }

        if (this.isSap())
            return true;
        if (cards.isSap())
            return false;

        if (this.getScore() > cards.getScore())
            return true;
        if (this.getScore() < cards.getScore())
            return false;

        return maxCard.compare(cards.maxCard);
    }

    public boolean isSap(){
    	if (myCard == null || myCard.length < 3) {
			return false;
		}
        int number = myCard[0].getNumber();
        for (int i = 1; i < myCard.length; i++){
            if (myCard[i].getNumber() != number)
                return false;
        }
        return true;
    }


    //Kiem tra xem du dieu kien lam chuong trong van moi hay ko
    public boolean checkForDealer(){
        if (getScore() == 10)
            return true;
        return false;
    }

    public String getName(){
        String ret = String.valueOf(getScore());
        ret = ret + " ";
        ret = ret + maxCard.getName();

        return ret;
    }

    public boolean isAnHu() {
    	Debug.trace("isAnHu" + myCard);
        if (myCard == null || myCard.length == 0){
            return false;
        }
        if (isSap() && myCard[0].getNumber() == 9){
            return true;
        }
        return false;
    }

	public String getHandCard() {
		if (myCard == null || myCard.length == 0) {
			return "";
		}
		String remain = "";
		for (Card card : myCard) {
			remain += card.getName() + ",";
		}
		return remain;
	}
	
	public String getCuocHu(){
		return "Sáp 9";
	}
	
	public static void main(String[] arg){
		
		PlayerCard pl1 = new PlayerCard();
		pl1.receiveCard(new Card(3), 0);
		pl1.receiveCard(new Card(1), 1);
		pl1.receiveCard(new Card(2), 2);
		pl1.sortCard();
		PlayerCard pl2 = new PlayerCard();
		pl2.receiveCard(new Card(0), 0);
		pl2.receiveCard(new Card(29), 1);
		pl2.receiveCard(new Card(23), 2);
		pl2.sortCard();
    	
    	boolean val = pl1.compare(pl2);
    	System.err.println("val:" + val + "|" + pl1.isSapARo() + "|" + pl2.isSapARo());
    }

	@Override
	public String toString() {
		return "PlayerCard [myCard=" + Arrays.toString(myCard) + ", maxCard=" + maxCard + "]";
	}

	//SapA + A rô
	public boolean isSapARo(){
		return maxCard.isAtRo() && isSap();
	}
}
