package game.server;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.Room;
import bitzero.server.entities.User;
import bitzero.server.entities.Zone;
import bitzero.server.extensions.BaseBZExtension;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import game.BaCayExtension;
import game.GameExtension;
import game.GameType;
import game.entities.CMD;
import game.entities.PlayerInfo;
import game.entities.RoomInfo;
import game.entities.ServerConstant;
import game.entities.TurnType;
import game.modules.bot.BotManager;
import game.modules.gameCheat.cmd.ReceiveConfigCards;
import game.modules.gameRoom.cmd.SendJoinRoomSuccess;
import game.modules.gameRoom.cmd.SendNewUserJoin;
import game.modules.gameRoom.cmd.SendUserExitRoom;
import game.server.cmd.ReceiveMaximize;
import game.server.cmd.SendKickFromRoom;
import game.server.cmd.SendQuitRoomSuccess;
import game.server.cmd.receive.ReceiveBet;
import game.server.cmd.receive.ReceiveChatMessage;
import game.server.cmd.receive.ReceiveInviteBetExtra;
import game.server.cmd.receive.ReceiveResponseInviteBetExtra;
import game.server.cmd.send.SendBet;
import game.server.cmd.send.SendInviteBetExtra;
import game.server.cmd.send.SendNotifyBetError;
import game.server.cmd.send.SendPlayerJoinChicken;
import game.server.cmd.send.SendPlayerShowCard;
import game.server.cmd.send.SendResponseInviteBetExtra;
import game.server.cmd.send.SendRoomInfo;
import game.utils.MetricLog;


public class GameServer extends BaseGameServer{




    private final class GameLoopTask implements Runnable {


        public void run() {

            try {

                gameLoop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public GameLoopTask() {
            super();
        }

    }

    private final Runnable gameLoopTask = new GameLoopTask();
    private ScheduledFuture<?> task;

    public static final int gsNoPlay = 0;
    public static final int gsPlay = 1;

    private final GameManager gameMgr = new GameManager();

    public final ArrayList<GamePlayer> playerList;
    private final ArrayList<GamePlayer> playersReady;// Danh sach nguoi choi cho van tiep theo -> Truoc moi van lam moi 1 lan
    public final ArrayList<GamePlayer> playersChickenJoined;// Danh sach nguoi choi cho van tiep theo -> Truoc moi van lam moi 1 lan

    public volatile int serverState;
    public volatile int groupIndex;
    public volatile boolean isTableDestroy = false;


    public BaseBZExtension baseSvr;
    private Room room = null;

    public static BaseBZExtension bs = ExtensionUtility.getExtension();

    private final Logger logger = LoggerFactory.getLogger("BaCayLogic");

    private void trace(String str) {
        String msg = str;
        if (room != null)
            msg = "{GameServer}: Room" + room.getZone().getName() + "-" + "Table " + room.getName() + "{" + room.getProperty("index").toString() + "}" + "-" + str;
        logger.info(msg);
    }

    public GameServer(Room ro) {
        super();
        room = ro;
        playerList = new ArrayList<GamePlayer>();
        for (int i = 0; i <= ServerConstant.MAX_PLAYER; i++) {
            playerList.add(new GamePlayer());
        }

        playersReady = new ArrayList<GamePlayer>();
        playersChickenJoined = new ArrayList<GamePlayer>();

        gameMgr.setGameServer(this);

        setServerState(gsNoPlay);
//        playerCount = 0;
    }


    public void init() {
        task = BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(gameLoopTask, 0, 1, TimeUnit.SECONDS);
    }

    public void destroy() {
        if (task != null) {
            task.cancel(true);
            task = null;
        }
    }

    public void reset() {
        gameMgr.setGameServer(this);
        setServerState(gsNoPlay);
//        serverState = gsNoPlay;
    }

    public int getRoomType() {
        return gameMgr.getRoomType();
    }
    
    /*public int getBetType() 
    {
        return gameMgr.getBetType();
    }*/

    public long getMoneyBet() {
        return gameMgr.getMoneyBet();
    }

    public synchronized int getServerState() {
        return serverState;
    }

    public void setServerState(int serverState) {
        Debug.trace("GameServer::setServerState------------------------: " + serverState);
        this.serverState = serverState;
    }

    public long getMinGold() {
        return gameMgr.getMinGold();
    }

    public int getPlayerLogin() {
        int nCount = 0;
        for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
            if (playerList.get(i).getPlayerStatus() != GamePlayer.psNO_LOGIN)
                nCount++;
        }
        return nCount;
    }

    public int getRoomId() {
        if (room != null)
            return room.getId();
        return -1;
    }

    public int getRoomIndex() {
        if (room != null)
            return (Integer) room.getProperty("index");
        return -1;
    }

    public void setRoom(Room r) {
        room = r;
    }

    public Room getRoom() {
        return room;
    }

    public GameManager getGameManager() {
        return gameMgr;
    }

    public void setRoomInfo(int moneyBet, int betType, int comission, int comissionJackpot) {
        gameMgr.setMoneyBet(moneyBet);
        gameMgr.setBetType(betType);
    }

    //    public void setRoomInfo2(int betIndex, long bet, long minGold, int type, int comission, int comissionJackpot, BaseBZExtension bs, int groupIndex)
    public void setRoomInfo2(long bet, long minGold, int type, int comission,
                             int comissionJackpot, BaseBZExtension bs,
                             int groupIndex, boolean cuoclon) {
//        trace ("setRoomInfo2>>>roomType " + type + " Moneybet " + bet);
//        gameMgr.setBetIndex(betIndex);

        gameMgr.roomInfo = ServerConstant.roomInfos.get(0).createWithBet((int) bet);


        gameMgr.setMoneyBet(bet);
        gameMgr.setRoomType(type);
        gameMgr.setMinGold(minGold);
        baseSvr = bs;
        this.groupIndex = groupIndex;

//        setRoomInfo(ServerConstant.roomInfos.get(0));
    }

    public void setRoomInfo(RoomInfo roomInfo) {
        gameMgr.roomInfo = roomInfo.clone();
    }


    public synchronized void onGameMessage(int cmdId, User user, DataCmd data) {
    		super.onGameMessage(cmdId, user, data);
        int nChair = user.getPlayerId();

        switch (cmdId) {
            case CMD.REQUEST_READY:
                onRequestGameStart();
                break;
            case CMD.REQUEST_VIEW_INFO:
                try {
                	gameMgr.processViewInfo(user);
				} catch (Exception e) {
					e.printStackTrace();
				}
                break;
            case CMD.RECEIVE_USER_REQ_QUIT_ROOM:
                gameMgr.processUserReqQuitRoom(user);
                break;
            case CMD.RECEIVE_USER_NOT_REQ_QUIT_ROOM:
                gameMgr.processUserNotReqQuitRoom(user);
                break;
            case CMD.USER_NOTIFY_CONNECT:
                gameMgr.processNotifyConnect(nChair);
                break;
            case CMD.NOTIFY_INFO_TABLE_RECONNECT:
                processInfoTableReconnect(user);
                break;
            case CMD.USER_MAXIMIZE:
                ReceiveMaximize _pCmd = new ReceiveMaximize(data);
                _pCmd.unpackData();
                gameMgr.processUserMaximize(user, _pCmd);
                break;
            //Tung
            case CMD.BET: {
                ReceiveBet cmd = new ReceiveBet(data);
                processPlayerBet(user, (int) cmd.gold);
            }
            break;
            case CMD.PLAYER_JOIN_CHICKEN:
                processPlayerJoinChicken(user);
                break;
            case CMD.INVITE_BET_EXTRA: {
                ReceiveInviteBetExtra cmd = new ReceiveInviteBetExtra(data);
                processInviteBetExtra(user, cmd.betIdx, cmd.chair);
            }
            break;
            case CMD.REPONSE_INVITE_BET_EXTRA: {
                ReceiveResponseInviteBetExtra cmd = new ReceiveResponseInviteBetExtra(data);
                processResponseInviteBetExtra(user, (byte) cmd.chair, cmd.isAccept);
            }
            break;
            case CMD.PLAYER_SHOW_CARD: {
                try {
                	processPlayerShowCard(user);
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
            break;

        }
    }


    public void processPlayerShowCard(User user) {
        if (!gameMgr.isTurn(TurnType.BET_END)) {
            return;
        }
        GamePlayer player = getPlayerByChair(user.getPlayerId());
        if (player.isShowCard) {
            return;
        }
        player.resetAfk();
        SendPlayerShowCard msg = new SendPlayerShowCard();
        msg.chair = (byte) (user.getPlayerId() - 1);
        msg.cardId = player.myCards.getMyCardID();
        sendMsg(msg);
        player.isShowCard = true;

        boolean cardAllUserIsShowed = true;
        for (int i = 0; i < playersReady.size(); i++) {
            GamePlayer player1 = playersReady.get(i);
            if (!player1.isShowCard) {
                cardAllUserIsShowed = false;
                break;
            }
        }
        if (cardAllUserIsShowed) {
            gameMgr.setTurn(TurnType.GAME_RESULT);
            gameMgr.doShowGameResult();
        }
    }

    private void processResponseInviteBetExtra(User user, byte chair, boolean isAccept) {
        int playerId = chair + 1;
        GamePlayer playerInvite = getPlayerByChair(playerId);
        GamePlayer playerReceive = getPlayerByChair(user.getPlayerId());
        playerReceive.resetAfk();
//        Debug.trace("GameServer::processResponseInviteBetExtra: " + playerInvite.getPlayingInfo().displayName + " -> " + playerReceive.getPlayingInfo().displayName);
        if (!playerInvite.isExistInvitation(playerReceive)) {
            //Ko ton tai loi moi
            return;
        }

        if (!gameMgr.isTurn(TurnType.BET_START)) {
            return;
        }

        if (!playerReceive.isBet()) {
            //Chua cuoc voi chuong
            return;
        }

        int betIdx = playerInvite.getBetExtraIdxFromInvitation(playerReceive);


        long betGold = gameMgr.getGoldBetExtra(betIdx);

        if (!playerInvite.checkGold(betGold + playerInvite.getBet())) {
            //Ko du tien
            return;
        }
        if (!playerReceive.checkGold(betGold + playerReceive.getBet())) {
            //Ko du tien
            return;
        }

        if (isAccept) {
            playerInvite.responseBetExtra(playerReceive, true, betIdx);
            playerReceive.responseBetExtra(playerInvite, true, betIdx);
            int roomId = room.getId();
            playerInvite.addGold(roomId, -betGold, GamePlayer.REASON_BET_EXTRA,0);
            playerReceive.addGold(roomId, -betGold, GamePlayer.REASON_BET_EXTRA,0);
        } else {
            playerInvite.responseBetExtra(playerReceive, false, -1);
        }

        //Send to invite
        {
            SendResponseInviteBetExtra msg = new SendResponseInviteBetExtra();
            msg.chair = (byte) (playerReceive.getPlayerId() - 1);
            msg.isAccept = isAccept;
            msg.betIdx = (byte) betIdx;
            sendMsg(msg, playerInvite.getUser());
        }


        //Send to receiver
        {
            SendResponseInviteBetExtra msg = new SendResponseInviteBetExtra();
            msg.chair = (byte) (playerInvite.getPlayerId() - 1);
            msg.isAccept = isAccept;
            msg.betIdx = (byte) betIdx;
            sendMsg(msg, playerReceive.getUser());
        }

//        Debug.info("GameServer::processResponseInviteBetExtra: room:" + room.getId() + " uid " + playerInvite.getPlayingInfo().getUId()
//                + " VS uid " + playerReceive.getPlayingInfo().getUId() + " -> " + betIdx);

    }

    public void processInviteBetExtra(User user, int betIdx, byte pChair) {
        if (!gameMgr.isTurn(TurnType.BET_START)) {
            return;
        }
        GamePlayer playerSent = getPlayerByChair(user.getPlayerId());
        playerSent.resetAfk();
        if (!playerSent.isBet()) {
            //Chua cuoc voi chuong
            return;
        }

        if (pChair == gameMgr.roomInfo.dealerPlayerId - 1) {
            //Ko duoc danh bien voi chuong
            return;
        }

        if (betIdx < 0 || betIdx >= gameMgr.roomInfo.betExtraList.length)
        {
            return;
        }

        long goldBet = gameMgr.getGoldBetExtra(betIdx);

        if (!playerSent.checkGold(goldBet + playerSent.getBet())) {
            //Thang gui thach dau ko du tien
            SendNotifyBetError msg = new SendNotifyBetError();
            msg.type = SendNotifyBetError.BET_WITH_ANOTHER_PLAYER;
            msg.reason = SendNotifyBetError.REASON_NOT_MONEY;
            sendMsg(msg, user);
            return;
        }


        GamePlayer playerReceive = getPlayerByChair(pChair + 1);

        long bet = Math.max(gameMgr.roomInfo.minBet, playerReceive.getBet());
        if (!playerReceive.checkGold(goldBet + bet)) {
            //Thang nhan thach dau ko du tien
            SendNotifyBetError msg = new SendNotifyBetError();
            msg.type = SendNotifyBetError.BET_WITH_ANOTHER_PLAYER;
            msg.reason = SendNotifyBetError.REASON_OPP_NOT_MONEY;
            sendMsg(msg, user);
            return;
        }


        if (playerReceive.isExistInvitation(playerSent)) {
            //Da co loi moi tu thang kia truoc
            if (playerReceive.getBetExtraIdxFromInvitation(playerSent) == betIdx) {
                // 2 thang cung moi chung 1 kieu -> thang moi sau chuyen thanh thang tra loi
                int chair = playerReceive.getPlayerId() - 1;
                processResponseInviteBetExtra(playerSent.user, (byte) chair, true);
            }
            return;
        }


        if (playerReceive.isBot() && playerSent.isBot()){
            //2 thang bot thi ko danh bien voi nhau
            return;
        }

//        Debug.trace("GameServer::processInviteBetExtra: " + playerSent.getPlayingInfo().displayName + " -> " + playerReceive.getPlayingInfo().displayName);

        SendInviteBetExtra msg = new SendInviteBetExtra();
        msg.chair = (byte) (user.getPlayerId() - 1);
        msg.betIdx = betIdx;
        sendMsg(msg, playerReceive.getUser());


        playerSent.inviteBetExtra(playerReceive, betIdx);


        //Xu ly cho bot
        if (playerReceive.isBot()){
            if (playerSent.isBot()){
                processResponseInviteBetExtra(playerReceive.user, (byte) (playerSent.getPlayerId() - 1), false);
            }
            else {
                boolean accept = new Random().nextBoolean();
                if(accept){
                    processResponseInviteBetExtra(playerReceive.user, (byte) (playerSent.getPlayerId() - 1), accept);
                }
            }
        }
    }

    public void processPlayerJoinChicken(User user) {
//        Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ":  START");
        if (!gameMgr.isTurn(TurnType.BET_START)) {
//            Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ":  1");
            return;
        }


        GamePlayer player = getPlayerByChair(user.getPlayerId());

        if (player.getPlayerStatus() != GamePlayer.psPLAY){
//            Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ":  2");
            return;
        }


        player.resetAfk();

        if (!player.checkGold(gameMgr.getBetInChicken() + player.getBet())) {
            SendNotifyBetError msg = new SendNotifyBetError();
            msg.type = SendNotifyBetError.BET_CHICKEN;
            msg.reason = SendNotifyBetError.REASON_NOT_MONEY;
            sendMsg(msg, user);
//            Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ":  3");
            return;
        }

        if (player.isChickenJoined){
//            Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ":  4");
            return;
        }


        player.isChickenJoined = true;
        player.addGold(room.getId(), -gameMgr.getBetInChicken(), GamePlayer.REASON_BET_CHICKEN,0);
        player.updateGoldWin(-gameMgr.getBetInChicken(), 0);
        playersChickenJoined.add(player);

        SendPlayerJoinChicken msg = new SendPlayerJoinChicken();
        msg.chair = (byte) (user.getPlayerId() - 1);
        msg.gold = playersChickenJoined.size() * gameMgr.getBetInChicken();
        sendMsg(msg);

//        Debug.info("GameServer::processPlayerJoinChicken room " + room.getId() + ": uid " + player.getPlayingInfo().getUId()
//                + " -> " + gameMgr.getBetInChicken() + " = " + player.getPlayingInfo().bean);
    }

    public void processPlayerBet(User user, int gold) {

        Debug.info("GameServer::processPlayerBet room " + room.getId() + ":  1");
        if (!gameMgr.isTurn(TurnType.BET_START)) {
            Debug.info("GameServer::processPlayerBet room " + room.getId() + ":  2");
            return;
        }

        if (user.getPlayerId() == gameMgr.roomInfo.dealerPlayerId) {
            //Chuong
            Debug.info("GameServer::processPlayerBet room " + room.getId() + ":  3");
            return;
        }

        if (gold < gameMgr.roomInfo.minBet || gold > gameMgr.roomInfo.maxBet) {
            //Muc cuoc ko dung
            Debug.info("GameServer::processPlayerBet room " + room.getId() + ":  4");
            return;
        }

        byte playerId = (byte) (user.getPlayerId());

        GamePlayer player = getPlayerByChair(playerId);
        if (player.isBet()) {
            return;
        }

        if (!player.checkGold(gold * gameMgr.roomInfo.hs10)){
            SendNotifyBetError msg = new SendNotifyBetError();
            msg.type = SendNotifyBetError.BET_WITH_DEALER;
            msg.reason = SendNotifyBetError.REASON_NOT_MONEY;
            sendMsg(msg, user);
            return;
        }

        if (player.getPlayerStatus() != GamePlayer.psPLAY){
            return;
        }


        player.resetAfk();
        doBet(playerId, gold);


    }

    public void doBet(byte playerId, long gold) {

        SendBet msg = new SendBet();
        msg.chair = (byte) (playerId - 1);
        msg.gold = gold;
        sendMsg(msg);

        GamePlayer player = getPlayerByChair(playerId);
        player.setBet(gold, room.getId());

//        Debug.info("GameServer::doBet room " + room.getId() + ":  uid" + player.getPlayingInfo().getUId() + " -> "
//                + (-gold) + " = " + player.getPlayingInfo().bean);
    }


    protected void onChatMessage(User user, String msg) {
    		super.onChatMessage(user, msg);
        playerList.get(user.getPlayerId()).timeDown = ServerConstant.USER_TIME_OUT;
    }

    public void setViewerChair(User user, int vChair) {
        user.setProperty(ServerConstant.PLAYER_VIEWCHAIR, vChair);
    }

    public void processWatcherTobeUser() {
        //SendNotifyViewerToUser cmd = new SendNotifyViewerToUser();
        for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
            GamePlayer pPlayer = getPlayerByChair(i);
            if (pPlayer != null) {
                if (pPlayer.getPlayerStatus() == GamePlayer.psLOOKON) {
                    pPlayer.setPlayerStatus(GamePlayer.psREADY);
                }
                //cmd.State[i-1] = pPlayer.getPlayerStatus();
            }
        }
        //sendMsg(cmd);
    }

    public GamePlayer getPlayerByChair(int playerId) {
        return playerList.get(playerId);
    }

    public synchronized void processUserExitRoom(User user, int chair, boolean disconnect) {
        trace("PROCESS " + user.getId() + " EXIT ROOM BEGIN.");
        if (chair >= 0) {
            GamePlayer pPlayer = getPlayerByChair(chair + 1);
            if (!disconnect || pPlayer.getPlayerStatus() != GamePlayer.psPLAY) {
                if (user != null) {
                    trace(user.getName() + " THOAT PHONG CHOI");
                    boolean isExit = onGameUserExit(user, chair + 1);
                    if (isExit) {
                        trace(user.getName() + "-EXIT ROOM SUCCESS.");
                        SendQuitRoomSuccess cmd = new SendQuitRoomSuccess();
                        sendMsg(cmd, user);
                    }
                }
            } else {
                trace("user " + user.getName() + " DISCONNECT KHI DANG CHOI");
                pPlayer.connect = false;
//                PlayerInfo pi = GameExtension.reloadPlayerInfo(user.getId());
//                PlayerInfo.saveToDB(pi);
            }
        }
        trace("PROCESS " + user.getId() + " EXIT ROOM END.");
    }

    public int getViewerChair(User user) {
        Object ob = (Integer) (user.getProperty(ServerConstant.PLAYER_VIEWCHAIR));
        if (ob != null)
            return (Integer) ob;
        else
            return -1;
    }

    private void processKickUserFromRoom(User user, int nChair) {
        logger.info(new Date().getTime() + " " + user.getName() + " THOAT PHONG CHOI");

        try {
            onGameUserExit(user, nChair);
            ExtensionUtility.instance().outRoom(user, room);
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        }
    }

    public void kickUserFromRoom(User user, int reason, int playerId) {
        {
            //Code cu
            if (user == null) {
                trace("kickUserFromRoom fail. user null ");
                return;
            }

            ExtensionUtility.instance().outRoom(user);
            SendKickFromRoom cmd = new SendKickFromRoom();
            cmd.chair = playerId - 1;
            cmd.reason = reason;
            cmd.uId = user.getId();
            sendMsgExceptMe(cmd, user);
            sendMsg(cmd, user);
            trace("kick " + user.getName() + " FromRoom success");
        }

//        for (int i = 0; i < playersReady.size(); i++){
//            if (playersReady.get(i).getPlayerId() == playerId){
//                playersReady.remove(i);
//                break;
//            }
//        }

        {
            //code moi
//            if (user == null || room == null)
//            {
//                Debug.trace("kickUserFromRoom fail. Chair ");
//                if (user == null)
//                    Debug.trace("kickUserFromRoom fail. user null ");
//                else
//                    Debug.trace("kickUserFromRoom fail. room null ");
//                return;
//            }
//            SendKickFromRoom cmd = new SendKickFromRoom();
//            cmd.chair = playerId - 1;
//            cmd.reason = (byte)reason;
//            sendMsg(cmd);
//            processKickUserFromRoom(user, playerId);
//            Debug.trace("kickUserFromRoom success");
//            Debug.trace("kickUserFromRoom end.");
        }

    }

    private BaCayExtension getExtension() {
        return (BaCayExtension) BitZeroServer.getInstance().getExtensionManager().getMainExtension();
    }

    public void onGameUserEnter(User user, int nChair) {

        try {
            trace(user.getName() + " onGameUserEnter");
            if (GameExtension.isReconnectRoom(user)) {
				Debug.info("REconnecting, ko gui tiep goi tin nay nua");
				return;
			}

            
            synchronized (this) {
                trace("GAME STATE " + serverState);
                int chair = user.getPlayerId();
                if (chair < 1 || chair > ServerConstant.MAX_PLAYER) {
                    trace(user.getName() + " chair invalid!! Chair " + chair);
                    return;
                }
                if (serverState == GameServer.gsPlay){
                    playerList.get(chair).loginViewer(user, gameMgr.roomInfo.minBet);
                }
                else {
                    playerList.get(chair).login(user, gameMgr.roomInfo.minBet);
                    addPlayer(playerList.get(chair));
                    gameMgr.processPlayerJoin(chair);
                }

                GamePlayer gamePlayer = playerList.get(chair);
                if (gamePlayer != null){
                    gamePlayer.setGameServer(this);
                }

//              playerCount ++ ;
                SendNewUserJoin cmd = new SendNewUserJoin();
                cmd.uChair = (byte) (chair - 1);
                cmd.setBaseInfo(playerList.get(chair).getPlayerInfo());
                sendMsgExceptMe(cmd, user);
                trace("Send joinRoomSuccess to " + user.getName());

                SendJoinRoomSuccess cmd1 = new SendJoinRoomSuccess();
                cmd1.uChair = (byte) (chair - 1);
                cmd1.comission = 101;//Game 3 cay ko dung cai nay
                cmd1.roomBet = gameMgr.getMoneyBet();
                cmd1.roomOwner = (byte) (gameMgr.getRoomOwner() - 1);
                cmd1.roomIndex = getRoomIndex();
                cmd1.roomId = getRoomId();
                cmd1.roomType = (byte) gameMgr.getRoomType();
                cmd1.roomOwnerId = room.getOwner().getId();
                
                cmd1.playerStatus = new byte[ServerConstant.MAX_PLAYER];
                cmd1.playerList = new PlayerInfo[ServerConstant.MAX_PLAYER];
                cmd1.goldTemp = new long[ServerConstant.MAX_PLAYER];
                cmd1.vip = new byte[ServerConstant.MAX_PLAYER];

                int rType = (Integer) room.getProperty(ServerConstant.ROOM_TYPE);
                cmd1.roomLock = (rType == 1) ? true : false;



                for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
                    GamePlayer player = playerList.get(i);
                    cmd1.playerStatus[i - 1] = (byte) player.getPlayerStatus();
                    cmd1.playerList[i - 1] = player.getPlayerInfo();
                    cmd1.goldTemp[i - 1] = player.getGoldTemp();
                }

                sendMsg(cmd1, user);

                SendRoomInfo msg = new SendRoomInfo();
                msg.roomInfo = gameMgr.roomInfo;
                sendMsg(msg, user);



                if (serverState == GameServer.gsNoPlay) {
                    gameMgr.choiceDealer();
                    gameMgr.checkAutoStartGame();
                    gameMgr.lockMiniGame();
//                    if (gameMgr.roomInfo.dealerPlayerId == GameManager.DEALER_UNKNOW || gameMgr.roomInfo.dealerPlayerId == GameManager.DEALER_ENOUGHT_MONEY){
//                        PlayerInfo.lockMiniGame(user.getId(), true);
//                    }
//                    int waiterTotal = 0;
//                    for (GamePlayer player : playerList){
//                        if (player == null){
//                            continue;
//                        }
//
//                        if (player.getPlayerStatus() != GamePlayer.psNO_LOGIN){
//                            waiterTotal++;
//                        }
//                    }
//                    if (waiterTotal > 1 && gameMgr.roomInfo.dealerPlayerId == GameManager.DEALER_UNKNOW || gameMgr.roomInfo.dealerPlayerId == GameManager.DEALER_ENOUGHT_MONEY ){
//                        gameMgr.lockMiniGame();
//                    }
                }
            }
            trace(user.getName() + " exit onGameUserEnter  ");
        } catch (Exception e) {
            e.printStackTrace();
            destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
        }

    }


    public boolean onGameUserExit(User user, int chair) {
        Debug.info(user.getName() + "- ON GAME USER EXIT " + chair);
        try {
            synchronized (this) {
                if (chair < 1 || chair > ServerConstant.MAX_PLAYER) {
                    trace("CHAIR INVALID " + chair);
                    checkDestroyRoom();
                    return false;
                }

                userQuitGameSuccess(user.getId());

                playerList.get(chair).logout();
//              if (playerCount > 0)
//                playerCount--;
                //trace(user.getName() + " THOAT GAME");


                gameMgr.processPlayerExit(chair);

                playerList.get(chair).clearInfo();
//              if (playerCount <= 0)
//                gameMgr.setRoomOwner(-1);
                //Update Money tu sms
                PlayerInfo pInfo = (PlayerInfo) user.getProperty(ServerConstant.PLAYER_INFO);
                if (pInfo != null) {
                    pInfo.setIsPlaying(GameType.NO_PLAYING);
                    pInfo.resetStateDisconnect();
                }

                SendUserExitRoom cmd = new SendUserExitRoom();
                //cmd.uId = user.getId();
                cmd.nChair = (byte) (chair - 1);
//                cmd.nChair = (byte)(chair);
                sendMsg(cmd);
                trace("SEND MSG USER EXIT ROOM");
                //ExtensionUtility.instance().outRoom(user);
                checkDestroyRoom();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonHandle.writeErrLog(e);
            destroyTable(SendKickFromRoom.Reason.eKICK_REASON_SYSTEM_ERROR);
        }
        checkDestroyRoom();
        return true;
    }

    public void updateViewer() {
//        try {
//            SendUpdateViewer cmd = new SendUpdateViewer();
//            int n = getNumViewer();
//            cmd.name = new String[n];
//            cmd.status = new byte[n];
//            int count = 0;
//            for (int i = 0; i < ServerConstant.MAX_ROOM_VIEWER; i++) {
//                if (viewerList[i] != null && viewerList[i].getViewerStatus() != GameViewer.vNO_LOGIN) {
//                    PlayerInfo pInfo = (PlayerInfo)viewerList[i].user.getProperty(ServerConstant.PLAYER_INFO);
//                    if(pInfo != null){
//                        cmd.name[count] = pInfo.displayName;
//                    }
//                    else{
//                        cmd.name[count] = viewerList[i].user.getName();
//                    }
//                    if (viewerList[i].getViewerStatus() ==
//                        GameViewer.vTookChair)
//                        cmd.status[count] = 1;
//                    else
//                        cmd.status[count] = 0;
//                    count++;
//                }
//            }
//            sendMsg(cmd);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    public void destroyTable(int reason) {
        playersReady.clear();
        isTableDestroy = true;
        for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
            GamePlayer gp = getPlayerByChair(i);
            if (gp != null) {
                PlayerInfo.setIsHold((int) gp.sInfo.uId, false);
                PlayerInfo.lockMiniGame(gp.sInfo.uId, false);
                PlayerInfo.resetGoldKept(gp.sInfo.uId);
                PlayerInfo.resetGoldKeptReal( gp.sInfo.uId);

                if (gp.isBot()){
                	BotManager.getInstance().addUid((int) gp.sInfo.uId);
                	continue;
                }

                if (gp.getUser() != null) {
                    kickUserFromRoom(gp.getUser(), reason, i);
                    PlayerInfo pInfo = gp.getPlayerInfo();
                    if (pInfo != null) {
                        MetricLog.writeDestroyTable((int) gp.sInfo.uId, getRoomId(),reason,this.gameMgr.codeVanChoi);
                    }
                    continue;
                }
                if (gp.user != null){
                    kickUserFromRoom(gp.user, reason, i);
                }
//                gameMgr.setGameState(GameManager.gmNoPlay);
            }
        }
        setServerState(gsNoPlay);
        MetricLog.destroy(getRoomId(), gameMgr.codeVanChoi);
        if (room != null) {
            Zone zone = room.getZone();
            zone.removeRoom(room);
        }
    }

    public void setUserChair(User user, int nChair) {
        user.setProperty(ServerConstant.PLAYER_CHAIR, nChair);
    }

    private void onRequestGameStart() {
        gameMgr.checkGameStart();
    }

    public synchronized boolean startGame() {
        gameMgr.kickPlayerDisconnect();
        //if (getPlayerLogin() == room.getPlayersList().size())
        {
            setServerState(gsPlay);
            Debug.info("GameServer::startGame room " + room.getId() + ":  ");

            int i;
            PlayerInfo pInfo;

            for (i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
            	GamePlayer gp = playerList.get(i);
                if (gp.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
                    pInfo = gp.getPlayerInfo();
                    if (pInfo == null /*|| user == null*/) {
                        continue;
                    }
                    PlayerInfo.setIsHold(pInfo.getUId(), true);
                    gp.setPlayerStatus(GamePlayer.psPLAY);
                    pInfo.setIsPlaying(ServerConstant.GAME_ID);
                    gp.Reset();
                    gp.setInfo(pInfo);
                    gp.startGame(gameMgr.getRoomType());
                    gp.connect = false;
                    
                    MetricLog.startGame(gp.getUserId(), gameMgr.getMoneyBet(), (int) getMinGoldRequired(gameMgr.getMoneyBet()), getRoomId(),gameMgr.codeVanChoi);
                } else {
                    trace("startGame>>>Player " + i + " clearInfo");
                    gp.Reset();
                    gp.isPlaying = false;
                    gp.isReady = false;
                    gp.clearInfo();
                }
            }
            return true;
        }
        /*else
          destroyTable();*/
        //return false;
    }

    public void endGame() {
        trace("ENDGAME");
        //Bo hold nguoi choi
        for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
            if (playerList.get(i).isPlaying) {
                PlayerInfo.setIsHold((int) playerList.get(i).sInfo.uId, false);
                PlayerInfo.increGame((int) playerList.get(i).sInfo.uId);
            }

            if (playerList.get(i).getPlayerStatus() != GamePlayer.psNO_LOGIN) {
                playerList.get(i).setPlayerStatus(GamePlayer.psREADY);
            }
        }
        setServerState(gsNoPlay);
    }


    public synchronized void gameLoop() {
        try {
            if (!isTableDestroy){
                gameMgr.gameLoop();
            }
        } catch (Exception e) {
            CommonHandle.writeWarnLog(e);
        }
    }

    public void sendMsg(BaseMsg cmd) {
        if (room != null) {
            baseSvr.send(cmd, room.getSessionList());
        }
    }

    public void sendMsg(BaseMsg cmd, User user) {
        if (user != null) {
            trace("send msg to " + user.getName());
            baseSvr.send(cmd, user);
        }
    }

    public void sendMsg(BaseMsg cmd, List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            sendMsg(cmd, users.get(i));
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void sendMsgExceptMe(BaseMsg cmd, User user) {
        if (user != null && room != null) {
            //baseSvr.sendExceptMe(cmd, room.getPlayersList(), user);
            List pList = new ArrayList();
            User u = null;
            for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
                if (playerList.get(i).getPlayerStatus() != GamePlayer.psNO_LOGIN) {
                    u = ExtensionUtility.globalUserManager.getUserById(playerList.get(i).getUserId());
                    if (u != null)
                        pList.add(u.getSession());
                }
            }
            if (pList.contains(user.getSession()))
                pList.remove(user.getSession());
            baseSvr.send(cmd, pList);
        }
    }

    public void sendMsgViewer(BaseMsg cmd) {
        for (int i = 1; i < ServerConstant.MAX_PLAYER + 1; i++) {
            int state = playerList.get(i).getPlayerStatus();
            if (state == GamePlayer.psLOOKON) {
                User user = ExtensionUtility.globalUserManager.getUserById(playerList.get(i).getUserId());
                if (user != null) {
                    baseSvr.send(cmd, user);
                }
            }
        }
    }

    public void processConfigCards(ReceiveConfigCards pCmd) {
        gameMgr.processConfigCard(pCmd);
    }

    public int getBetIndex() {
        return gameMgr.getBetIndex();
    }

    private boolean hasOnlyOwner() {
        return (room.getPlayersList().size() == 1);
    }

    public int autoJoinRoomDisconnect(User user, GamePlayer gp) {
		return gameMgr.infoTableReconnect(user, gp);
	}

    /**
     * thong tin ban choi khi reconnect *
     */
    private void processInfoTableReconnect(User user) {
        if (user == null) return;
        GamePlayer gp = getPlayerByUserId(user.getId());
        gameMgr.infoTableReconnect(user,gp);
    }

    public GamePlayer getPlayerById(int userId) {
        GamePlayer player = null;
        for (int i = 0; i < playerList.size(); i++) {
            player = playerList.get(i);
            if (player.getUserId() == userId) {
                return player;
            }
        }
        return null;
    }

    /**
     * @return chair of roomowner
     * @author David Ngo
     */
    public int getRoomOwner() {
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).getUser().getId() == getRoom().getOwner().getId()) {
                return i;
            }
        }
        return -1;
    }

    public void refreshPlayerReadyList() {
        playersReady.clear();
        playersChickenJoined.clear();
        for (int i = 0; i < playerList.size(); i++) {
            GamePlayer player = playerList.get(i);
            if (player.isLogin()) {
                player.resetGame();
                addPlayer(player);
            }
        }
    }

    private void addPlayer(GamePlayer player){
        playersReady.add(player);

        String id = "UNKNOWN";

        if (player.user != null){
            id = String.valueOf(player.user.getId());
        }
        else if (player.getUser() != null){
            id = String.valueOf(player.getUser().getId());
        }

        Debug.info("GameServer::addPlayer room " + room.getId() + ":  " + id);
    }

    public boolean removePlayer(GamePlayer player){
        synchronized (playersReady){
            for (int i = 0; i < playersReady.size(); i++){
                if (playersReady.get(i) == player){
                    playersReady.remove(i);

                    String id = "UNKNOWN";

                    if (player.user != null){
                        id = String.valueOf(player.user.getId());
                    }
                    else if (player.getUser() != null){
                        id = String.valueOf(player.getUser().getId());
                    }

                    Debug.info("GameServer::removePlayer room " + room.getId() + ":  " + id);
                    return true;
                }
            }
        }

        return false;
    }

    public int getPlayerCount() {
        return playersReady.size();
    }

    public List<GamePlayer> getPlayerReadyList() {
        return playersReady;
    }
    public static long getMinGoldRequired(long bet){
    	return RoomInfo.requiredMinGold(bet);
    }
    public GamePlayer getPlayerByUserId(long userId){
    	int i = 0;
    	for (GamePlayer gamePlayer : playerList) {
			if (gamePlayer.getUserId() == userId) {
				gamePlayer.setPlayerId(i);
				return gamePlayer;
			}
			i++;
		}
    	return null;
    }

    public String getPlayers() {
        String ret = "";
        for (GamePlayer gamePlayer : playerList){
            if (gamePlayer == null){
                continue;
            }
            if (gamePlayer.getPlayerInfo() == null)
                continue;
            ret += gamePlayer.getUserId() + ": " + gamePlayer.getPlayerInfo().displayName + ", ";

        }
        return ret;
    }

	@Override
	public String toString() {
		return "GameServer [playerList=" + playerList + ", playersReady=" + playersReady + ", serverState="
				+ serverState + "]";
	}
    
	//chi 1 bot vao phong
	public boolean canBotJoin(){
		int m = 0;
		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = getPlayerByChair(i);
			if (gp != null && gp.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
				PlayerInfo pInfo = gp.getPlayerInfo();
				if (pInfo != null && pInfo.isBot()) {
					m++;
				}
			}
		}
		return m < 1;
	}
	
	public void checkDestroyRoom(){
		int n = 0;
		int m = 0;
		for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = getPlayerByChair(i);
			if (gp != null) {
				if (gp.getPlayerStatus() != GamePlayer.psNO_LOGIN) {
					n++;
				}
				PlayerInfo pInfo = gp.getPlayerInfo();
				if (pInfo != null && pInfo.isBot()) {
					m++;
				}
			}
		}
		if (m == n) {
			room.getZone().removeRoom(room);
			Debug.trace("Huy ban choi vi chi con lai bot ! ==>" + room.getId());
			for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
				GamePlayer gp = getPlayerByChair(i);
				if (gp != null) {
					PlayerInfo pInfo = gp.getPlayerInfo();
					if (pInfo != null && pInfo.isBot()) {
						m++;
						BotManager.getInstance().addUid(pInfo.getUId());
					}
				}
			}
		}
	}
	public GameExtension getGameExt(){
		return ((GameExtension)this.bs);
	}
	
    public void doUserReqQuitRoom() {
        for (int i = 1; i <= ServerConstant.MAX_PLAYER; i++) {
			GamePlayer gp = getPlayerByChair(i);
			if (gp != null && gp.getUser() != null) {
            	if (!BotManager.isBot(gp.getUser().getId())) {
                	getGameExt().checkValidSession(gp.getUser(), 2);
				}
			}
		}
    }
}

